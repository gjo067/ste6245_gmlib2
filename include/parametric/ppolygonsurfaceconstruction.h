#ifndef GM2_PPOLYGONSURFACECONSTRUCTION_H
#define GM2_PPOLYGONSURFACECONSTRUCTION_H

#include "polygon_surface_constructions/utils.h"
#include "parametricobject.h"

namespace gmlib2::parametric
{

  // forward declaration
  template <typename SpaceObjectBase_T, size_t ParameterSpaceFrameDim>
  class PPolygonSurfaceConstruction;



  namespace objecttype
  {
    struct ppolygonsurfaceconstruction_tag {
    };
  }   // namespace objecttype

  namespace detail
  {

    template <typename ParametricSpaceInfo_T, typename SpaceObjectEmbedBase_T>
    class Sampler<ParametricSpaceInfo_T, objecttype::ppolygonsurfaceconstruction_tag,
                  SpaceObjectEmbedBase_T> {
    public:
      using Unit_Type          = typename SpaceObjectEmbedBase_T::Unit_Type;
      using ParameterSpaceInfo = ParametricSpaceInfo_T;
      using ParameterSpace     = spaces::ProjectiveSpace<ParametricSpaceInfo_T>;
      static constexpr auto PSpace_VectorDim = ParameterSpace::VectorDim;
      using PSpacePoint = typename ParameterSpace::Point_Type;
      using PSizeArray  = std::array<size_t, PSpace_VectorDim>;
      using PBoolArray  = std::array<bool, PSpace_VectorDim>;

      using EvaluationResult
        = DVectorT<typename SpaceObjectEmbedBase_T::VectorH_Type>;
      using SampleResult = DVectorT<EvaluationResult>;

      template <typename Object_T>
      static auto sample(Object_T* pobj, const PSpacePoint&     /*par_start*/,
                         const PSpacePoint& /*par_end*/, size_t samples_M)
      {

        using PSC_POBJ_TYPE = PPolygonSurfaceConstruction<
                                   SpaceObjectEmbedBase_T, PSpace_VectorDim>;

//        static_assert(
//          std::is_same_v<Object_T, PSC_POBJ_TYPE>,
//          "Input object error; Obejct_T* pobj, must be of "
//          "PPolygonSurfaceConstruction type.");



        constexpr auto no_der
          = utils::initStaticContainer<PSizeArray, PSpace_VectorDim>(0UL);
        constexpr auto from_left
          = utils::initStaticContainer<PBoolArray, PSpace_VectorDim>(true);


        auto* psc_pobj = reinterpret_cast<const PSC_POBJ_TYPE*>(pobj);

        const auto polygon_2d = psc_pobj->polygon2D();
        const auto N = int(polygon_2d.size());
        const auto M = int(samples_M);


        size_t no_samples = 0;  // M == 0;
        if( M == 1 ) no_samples = 1;
        else {
          no_samples = 1;
          for (int k = 2; k <= M; ++k) no_samples += N * (k - 1);
        }


        SampleResult p(no_samples);

        // Sample in each of the control point vertices
//        auto vert_pars = DVectorT<DVectorT<Unit_Type>{N,DVectorT{N,Unit_Type{0}}};
        auto par = DVectorT<DVectorT<Unit_Type>>(N,DVectorT<Unit_Type>(N,Unit_Type{0}));
        for (size_t i = 0; i < N; ++i) {
          par[i][i] = 1.0;
        }

//        const auto vert_par_center
        const auto center
            = DVectorT<Unit_Type>(N,Unit_Type{1}/Unit_Type{N});

//          = utils::initStaticContainer<PSpacePoint, PSpace_VectorDim>(
//            1.0 / typename ParameterSpace::Unit_Type{N});



        const auto center_eval_par = blaze::inner(polygon_2d, center);

        p[0] = pobj->evaluateLocal(center_eval_par, no_der, from_left);


//          std::cout << "Polygon sampling; no. samples: " << no_samples << std::endl;

//          std::cout << "Polygon parametric vertex points; [center, 0, ..., N-1]: " << std::endl
//                    << center << std::endl;
//          for( const auto& par_ : par )
//            std::cout << par_ << std::endl;

//          std::cout << "Polygon sampling indexing for N,M: " << N << "," << M << std::endl;
        for( int m = 0; m < M; ++m ) {

          const int T = m-1;

          int r_o;
          if( m == 0 ) r_o = 0;
          else if( m == 1 ) r_o = 1;
          else {
            r_o = 1;
            for( int k = 2; k <= m; ++k ) {
              r_o += N * (k-1);
            }
          }

          for( int n = 0; n < (m==0?1:N); ++n) {

            const int t_o = r_o + n * T + n;



            for( int t_i = 0; t_i <= T; ++t_i ) {

              const int idx = t_o + t_i;
//              std::cout << "idx: " << idx << " ";
//                std::cout << "  n,m: " << n << "," << m << std::endl;
//                std::cout << "     -> r_o: " << r_o
//                          << ", t_o: " << t_o << ", idx: " << idx << std::endl;

              const double u = 1.0 - ((1.0 / double(M-1)) * m);
              const double w = (1.0 / double(M-1)) * t_i;
              const double v = 1.0 - (u+w);

//                std::cout << "     -> u,v,w: "
//                          << "(" << u << "," << v << "," << w << ")"
//                          << std::endl;
              const auto poly_par = blaze::evaluate(center * u + par[n] * v + par[n==N-1?0:n+1] * w);

//                std::cout << "     -> poly_par: " << std::endl << poly_par << std::endl;

              p[idx] = pobj->evaluateLocal(blaze::inner(polygon_2d,poly_par), no_der, from_left);
//              std::cout << " p[idx]: " << p[idx] << std::endl;
            }
          }
        }

        return p;
      }
    };
  }   // namespace detail




//  template <typename SpaceObjectBase_T    = SpaceObject<>,
//            size_t ParameterSpaceFrameDim = 2>
//  using PPolygonSurfaceConstruction
//    = ParametricObject<spaces::ParameterFVSpaceInfo<ParameterSpaceFrameDim, 2>,
//                       objecttype::ppolygonsurfaceconstruction_tag, SpaceObjectBase_T>;



  template <typename SpaceObjectBase_T    = ProjectiveSpaceObject<>,
            size_t ParameterSpaceFrameDim = 2>
  class PPolygonSurfaceConstruction
    : public ParametricObject<
        spaces::ParameterFVSpaceInfo<ParameterSpaceFrameDim, 2>,
        objecttype::ppolygonsurfaceconstruction_tag, SpaceObjectBase_T> {

    using Base = ParametricObject<
      spaces::ParameterFVSpaceInfo<ParameterSpaceFrameDim, 2>,
      objecttype::ppolygonsurfaceconstruction_tag, SpaceObjectBase_T>;

  public:
    public:
      // Dimensions
      static constexpr auto VectorDim  = Base::VectorDim;
      static constexpr auto PVectorDim = Base::PSpace_VectorDim;

      // Inherited Types
      using Unit_Type        = typename Base::Unit_Type;
      using EvaluationResult = typename Base::EvaluationResult;
      using Point            = typename Base::Point_Type;
      using Vector           = typename Base::Vector_Type;
      using VectorH          = typename Base::VectorH_Type;

      // Inherited Parameter space types
      using PSpacePoint = typename Base::PSpace_Point_Type;
      using PSizeArray  = typename Base::PSpace_SizeArray_Type;
      using PBoolArray  = typename Base::PSpace_BoolArray_Type;

    // Polygon construction types
      using Polygon2D = DVectorT<PSpacePoint>;


      const Polygon2D& polygon2D() const { return m_polygon; }
      size_t           sides() const { return m_polygon.size(); }

      template <typename... Ts>
      PPolygonSurfaceConstruction(Polygon2D&& polygon, Ts&&... ts)
        : Base(std::forward<Ts>(ts)...), m_polygon{std::forward<Polygon2D>(polygon)}
      {
      }

      template <typename... Ts>
      PPolygonSurfaceConstruction(size_t N, Ts&&... ts)
        : Base(std::forward<Ts>(ts)...),
          m_polygon(utils::parametric::psc::generateRegularPolygon2D<>(N))
      {
      }




      // Member(s)
    private:
      Polygon2D m_polygon;
  };

}   // namespace gmlib2::parametric

#endif   // GM2_PPOLYGONSURFACECONSTRUCTION_H

















