#ifndef GM2_PSURFACE_H
#define GM2_PSURFACE_H

#include "parametricobject.h"

namespace gmlib2::parametric
{
  namespace objecttype
  {
    struct psurface_tag {
    };
  }   // namespace objecttype

  namespace detail
  {

    template <typename ParametricSpaceInfo_T, typename SpaceObjectEmbedBase_T>
    class Sampler<ParametricSpaceInfo_T, objecttype::psurface_tag,
                  SpaceObjectEmbedBase_T> {
    public:
      using Unit_Type          = typename SpaceObjectEmbedBase_T::Unit_Type;
      using ParameterSpaceInfo = ParametricSpaceInfo_T;
      using ParameterSpace     = spaces::ProjectiveSpace<ParametricSpaceInfo_T>;
      static constexpr size_t PSpace_VectorDim = ParameterSpace::VectorDim;
      using PSpacePoint = typename ParameterSpace::Point_Type;
      using PSizeArray  = std::array<size_t, PSpace_VectorDim>;
      using PBoolArray  = std::array<bool, PSpace_VectorDim>;

      using EvaluationResult
        = DMatrixT<typename SpaceObjectEmbedBase_T::VectorH_Type>;
      using SampleResult = DMatrixT<EvaluationResult>;

      template <typename Object_T>
      static auto sample(Object_T* pobj, const PSpacePoint& par_start,
                         const PSpacePoint& par_end,
                         const PSizeArray&  no_samples,
                         const PSizeArray&  no_derivatives)
      {
        const auto m1  = no_samples[0];
        const auto m2  = no_samples[1];
        const auto s_u = par_start[0];
        const auto s_v = par_start[1];
        const auto e_u = par_end[0];
        const auto e_v = par_end[1];

        Unit_Type du = (e_u - s_u) / (m1 - 1);
        Unit_Type dv = (e_v - s_v) / (m2 - 1);

        SampleResult p(m1, m2);

        for (size_t i = 0; i < m1 - 1; i++) {
          Unit_Type u = s_u + i * du;
          for (size_t j = 0; j < m2 - 1; j++) {
            p(i, j) = pobj->evaluateLocal(PSpacePoint{u, s_v + j * dv},
                                          no_derivatives, {{true, true}});
          }
          p(i, m2 - 1) = pobj->evaluateLocal(PSpacePoint{u, e_v},
                                             no_derivatives, {{true, false}});
        }

        for (size_t j = 0; j < m2 - 1; j++) {
          p(m1 - 1, j) = pobj->evaluateLocal(PSpacePoint{e_u, s_v + j * dv},
                                             no_derivatives, {{false, true}});
        }

        p(m1 - 1, m2 - 1) = pobj->evaluateLocal(
          PSpacePoint{e_u, e_v}, no_derivatives, {{false, false}});

        return p;
      }
    };
  }   // namespace detail


  template <typename SpaceObjectBase_T    = ProjectiveSpaceObject<>,
            size_t ParameterSpaceFrameDim = 2>
  using PSurface
    = ParametricObject<spaces::ParameterFVSpaceInfo<ParameterSpaceFrameDim, 2>,
                       objecttype::psurface_tag, SpaceObjectBase_T>;

}   // namespace gmlib2::parametric

#endif   // GM2_PSURFACE_H
