#ifndef GM2_PPOINT_H
#define GM2_PPOINT_H

#include "parametricobject.h"

namespace gmlib2::parametric
{

  namespace objecttype
  {
    struct ppoint_tag { };
  }   // namespace objecttype


  namespace detail {

    template <typename ParametricSpaceInfo_T, typename SpaceObjectEmbedBase_T>
    class Sampler<ParametricSpaceInfo_T, objecttype::ppoint_tag,
                  SpaceObjectEmbedBase_T> {
    public:
      using EvaluationResult = typename SpaceObjectEmbedBase_T::VectorH_Type;
      using SampleResult     = typename SpaceObjectEmbedBase_T::VectorH_Type;

      using ParameterSpace     = spaces::ProjectiveSpace<ParametricSpaceInfo_T>;
      using PSpacePoint = typename ParameterSpace::Point_Type;

      template <typename Object_T>
      static SampleResult sample(Object_T* obj, const PSpacePoint&,
                                 const PSpacePoint&)
      {
        return SampleResult(obj->evaluateLocal());
      }
    };

  } // namespace detail



  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 0>
  class PPoint : public ParametricObject<
                   spaces::ParameterFVSpaceInfo<ParametricSpaceFrameDim_T, 0>,
                   objecttype::ppoint_tag, SpaceObjectEmbedBase_T> {
    using Base = ParametricObject<
      spaces::ParameterFVSpaceInfo<ParametricSpaceFrameDim_T, 0>,
      objecttype::ppoint_tag, SpaceObjectEmbedBase_T>;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using Point            = typename Base::Point_Type;
    using VectorH          = typename Base::VectorH_Type;
    using EvaluationResult = typename Base::EvaluationResult;

    // ParametricSpaceObject types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructors
    using Base::Base;

    // Members
    const VectorH m_pt{0.0, 0.0, 0.0, 1.0};

    // ParameterSpaceObject interface
  public:
    PBoolArray       isClosed() const final;
    PSpacePoint      startParameters() const final;
    PSpacePoint      endParameters() const final;
    EvaluationResult evaluate(const PSpacePoint& par    = PSpacePoint(),
                              const PSizeArray&  no_der = PSizeArray(),
                              const PBoolArray&  from_left
                              = PBoolArray()) const final;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPoint<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PBoolArray
  PPoint<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return PBoolArray();   // detail::initStaticContainer<PBoolArray&,
                           // PVectorDim>(false);
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPoint<SpaceObjectEmbedBase_T,
                  ParametricSpaceFrameDim_T>::PSpacePoint
  PPoint<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::startParameters()
    const
  {

    return PSpacePoint();
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPoint<SpaceObjectEmbedBase_T,
                  ParametricSpaceFrameDim_T>::PSpacePoint
  PPoint<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::endParameters()
    const
  {
    return PSpacePoint();
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPoint<SpaceObjectEmbedBase_T,
                  ParametricSpaceFrameDim_T>::EvaluationResult
  PPoint<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint&, const PSizeArray&, const PBoolArray&) const
  {
    return m_pt;
  }

}   // namespace gmlib2::parametric

#endif   // GM2_PCURVE_H
