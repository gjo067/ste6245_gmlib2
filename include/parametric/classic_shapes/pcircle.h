#ifndef GM2_PCIRCLE_H
#define GM2_PCIRCLE_H


#include "../pcurve.h"
#include "../../core/utils.h"

#include <iostream>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 1>
  class PCircle
    : public PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor
    PCircle(Unit_Type radius = Unit_Type{3.0}) : m_radius{radius} {}

    template <typename... Ts>
    PCircle(Unit_Type radius, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_radius{radius}
    {
    }

    // Members
    Unit_Type m_radius;

    // PCurve interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCircle<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PBoolArray
  PCircle<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{true}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCircle<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PSpacePoint
  PCircle<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::startParameters()
    const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCircle<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PSpacePoint
  PCircle<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::endParameters()
    const
  {
    return PSpacePoint{2 * M_PI};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PCircle<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::EvaluationResult
  PCircle<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint& par, const PSizeArray& no_der,
    const PBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    const Unit_Type ct = m_radius * std::cos(t);
    const Unit_Type st = m_radius * std::sin(t);

    p[0] = {ct, st, 0, 1};
    if (no_derivatives) p[1] = {-st, ct, 0, 0};
    if (no_derivatives > 1) p[2] = -p[0];
    if (no_derivatives > 2) p[3] = -p[1];

    return p;
  }

}   // namespace gmlib2::parametric


#endif   // GM2_PCIRCLE_H
