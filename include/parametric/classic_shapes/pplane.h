#ifndef GM2_PPLANE_H
#define GM2_PPLANE_H


#include "../psurface.h"
#include "../../core/utils.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PPlane
    : public PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor(s)
    template <typename... Ts>
    PPlane(const Point&  origin   = Point{0.0, 0.0, 0.0},
           const Vector& u_vector = Vector{1.0, 0.0, 0.0},
           const Vector& v_vector = Vector{0.0, 1.0, 0.0})
      : m_pt{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, 1.0)},
        m_u{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          u_vector, 0.0)},
        m_v{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          v_vector, 0.0)}
    {
    }

    template <typename... Ts>
    PPlane(const Point& origin, const Vector& u_vector, const Vector& v_vector,
           Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_pt{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, 1.0)},
        m_u{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          u_vector, 0.0)},
        m_v{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          v_vector, 0.0)}
    {
    }
    ~PPlane() override = default;

    // Members
    VectorH m_pt;
    VectorH m_u;
    VectorH m_v;


    // TPSurf interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };





  template <typename SpaceObjectBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::PBoolArray
  PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false, false}};
  }

  template <typename SpaceObjectBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::PSpacePoint
  PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::PSpacePoint
  PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{1, 1};
  }

  template <typename SpaceObjectBase_T, size_t ParametricSpaceFrameDim_T>
  typename PPlane<SpaceObjectBase_T,
                  ParametricSpaceFrameDim_T>::EvaluationResult
  PPlane<SpaceObjectBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint& par, const PSizeArray& no_der,
    const PBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    // S
    p(0, 0) = m_pt + u * m_u + v * m_v;

    if (no_der_u)   // Su
      p(1, 0) = m_u;
    if (no_der_v)   // Sv
      p(0, 1) = m_v;
    if (no_der_u and no_der_v)   // Suv
      p(1, 1) = {0.0, 0.0, 0.0, 0.0};

    return p;
  }





}   // namespace gmlib2::parametric

#endif   // GM2_PPLANE_H
