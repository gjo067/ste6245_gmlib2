#ifndef GM2_PTORUS_H
#define GM2_PTORUS_H



#include "../parametricobject.h"
#include "../../core/utils.h"


namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T    = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PTorus : public PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
  public:
    using Base = PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor(s)
    PTorus(Unit_Type wheelradius = Unit_Type{3.0},
           Unit_Type tuberadius1 = Unit_Type{1.0},
           Unit_Type tuberadius2 = Unit_Type{1.0})
      : m_wheelradius{wheelradius}, m_tuberadius1{tuberadius1}, m_tuberadius2{
                                                                  tuberadius2}
    {
    }

    template <typename... Ts>
    PTorus(Unit_Type wheelradius, Unit_Type tuberadius1, Unit_Type tuberadius2,
           Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_wheelradius{wheelradius},
        m_tuberadius1{tuberadius1}, m_tuberadius2{tuberadius2}
    {
    }

    // Members
    Unit_Type m_wheelradius;
    Unit_Type m_tuberadius1;
    Unit_Type m_tuberadius2;

    // PSurf interface
  public:
    PBoolArray isClosed() const override;
    PSpacePoint    startParameters() const override;
    PSpacePoint    endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PBoolArray
  PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{true, true}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PSpacePoint
  PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PSpacePoint
  PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{2 * M_PI, 2 * M_PI};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::EvaluationResult
  PTorus<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint& par, const PSizeArray& no_der,
    const PBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    Unit_Type su   = std::sin(u);
    Unit_Type sv   = std::sin(v);
    Unit_Type cu   = std::cos(u);
    Unit_Type cv   = std::cos(v);
    Unit_Type bcva = m_tuberadius1 * cv + m_wheelradius;
    //        Unit_Type cucv = m_tuberad1 * cu * cv;
    Unit_Type cusv = m_tuberadius1 * cu * sv;
    //        Unit_Type sucv = m_tuberad1 * su * cv;
    Unit_Type susv = m_tuberadius1 * su * sv;
    sv *= m_tuberadius2;
    cv *= m_tuberadius2;
    cu *= bcva;
    su *= bcva;

    p(0, 0) = {cu, su, sv, 1};                                  // S
    if (no_der_u) p(1, 0) = {-su, cu, 0, 0};                    // Su
    if (no_der_v) p(0, 1) = {-cusv, -susv, cv, 0};              // Sv
    if (no_der_u and no_der_v) p(1, 1) = {susv, -cusv, 0, 0};   // Suv

    return p;
  }


}   // namespace gmlib2::parametric


#endif   // GM2_PTORUS_H
