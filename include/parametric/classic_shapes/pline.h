#ifndef GM_PLINE_H
#define GM_PLINE_H



#include "../pcurve.h"
#include "../../core/utils.h"

#include <iostream>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 1>
  class PLine
    : public PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    static constexpr auto VectorDim = Base::VectorDim;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor
    PLine(const Point&  origin    = Point(0.0),    // Point{0.0, 0.0, 0.0},
          const Vector& direction = Vector(1.0))   // Vector{1.0, 0.0, 0.0})
      : m_pt(utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, 1.0)),
        m_v(utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
          direction, 0.0))
    {
    }

    template <typename... Ts>
    PLine(const Point& origin, const Vector& direction, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_pt(utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, 1.0)),
        m_v(utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
          direction, 0.0))
    {
    }

    // Members
    VectorH m_pt;
    VectorH m_v;

    // PCurve interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PBoolArray
  PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PSpacePoint
  PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::startParameters()
    const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::PSpacePoint
  PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::endParameters()
    const
  {
    return PSpacePoint{1};
  }


  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PLine<SpaceObjectEmbedBase_T,
                 ParametricSpaceFrameDim_T>::EvaluationResult
  PLine<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint& par, const PSizeArray& no_der,
    const PBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    p[0] = m_pt + t * m_v;
    if (no_derivatives) p[1] = m_v;
    for (size_t i = 2; i < no_derivatives + 1; ++i) p[i] = {0.0, 0.0, 0.0, 0.0};

    return p;
  }

}   // namespace gmlib2::parametric



#endif   // GM_PLINE_H
