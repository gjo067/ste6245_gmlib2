#ifndef GM2_PSPHERE_H
#define GM2_PSPHERE_H


#include "../parametricobject.h"
#include "../../core/utils.h"

namespace gmlib2::parametric
{


  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PSphere
    : public PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor(s)
    PSphere(Unit_Type radius = Unit_Type{1}) : m_radius{radius} {}

    template <typename... Ts>
    PSphere(Unit_Type radius, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_radius{radius}
    {
    }

    // Members
    Unit_Type m_radius;

    // TPSurf interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };





  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PSphere<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PBoolArray
  PSphere<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{true, false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PSphere<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PSpacePoint
  PSphere<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::startParameters()
    const
  {
    return PSpacePoint{0, -M_PI_2};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PSphere<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::PSpacePoint
  PSphere<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::endParameters()
    const
  {
    return PSpacePoint{2 * M_PI, M_PI_2};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PSphere<SpaceObjectEmbedBase_T,
                   ParametricSpaceFrameDim_T>::EvaluationResult
  PSphere<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::evaluate(
    const PSpacePoint& par, const PSizeArray& no_der,
    const PBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    const Unit_Type r    = m_radius;
    const Unit_Type cu   = std::cos(u);
    const Unit_Type cv   = r * std::cos(v);
    const Unit_Type su   = std::sin(u);
    const Unit_Type sv   = r * std::sin(v);
    const Unit_Type cucv = cu * cv;
    const Unit_Type cusv = cu * sv;
    const Unit_Type sucv = su * cv;
    const Unit_Type susv = su * sv;

    p(0, 0) = {cucv, sucv, sv, 1};                              // S
    if (no_der_u) p(1, 0) = {-sucv, cucv, 0, 0};                // Su
    if (no_der_v) p(0, 1) = {-cusv, -susv, cv, 0};              // Sv
    if (no_der_u and no_der_v) p(1, 1) = {susv, -cusv, 0, 0};   // Suv

    return p;
  }





}   // namespace gmlib2::parametric

#endif   // GM2_PSPHERE_H
