#ifndef GM2_PCURVE_H
#define GM2_PCURVE_H

#include "parametricobject.h"

namespace gmlib2::parametric
{
  namespace objecttype
  {
    struct pcurve_tag {
    };
  }   // namespace objecttype

  namespace detail
  {

    ////////////////////////////////////
    /// Default Parametric Curve sampler
    ///
    template <typename ParametricSpaceInfo_T, typename SpaceObjectEmbedBase_T>
    class Sampler<ParametricSpaceInfo_T, objecttype::pcurve_tag,
                  SpaceObjectEmbedBase_T> {
    public:
      // Paramter space types
      using ParameterSpaceInfo = ParametricSpaceInfo_T;
      using ParameterSpace     = spaces::ProjectiveSpace<ParametricSpaceInfo_T>;
      static constexpr size_t PSpace_VectorDim = ParameterSpace::VectorDim;
      using PSpacePoint = typename ParameterSpace::Point_Type;
      using PSizeArray  = std::array<size_t, PSpace_VectorDim>;

      // Data types
      using Unit_Type = typename SpaceObjectEmbedBase_T::Unit_Type;
      using EvaluationResult
        = DVectorT<typename SpaceObjectEmbedBase_T::VectorH_Type>;
      using SampleResult = DVectorT<EvaluationResult>;

      // Sample methods
      template <typename Object_T>
      static auto sample(Object_T* pobj, const PSpacePoint& par_start,
                         const PSpacePoint& par_end,
                         const PSizeArray&  no_samples,
                         const PSizeArray&  no_derivatives)
      {
        const auto m = no_samples[0];
        const auto s = par_start[0];
        const auto e = par_end[0];

        Unit_Type dt = (e - s) / (m - 1);

        SampleResult p(m);

        for (size_t i = 0; i < m - 1; i++)
          p[i] = pobj->evaluateLocal(PSpacePoint{s + i * dt}, no_derivatives,
                                     {{true}});
        p[m - 1] = pobj->evaluateLocal(par_end, no_derivatives, {{false}});

        return p;
      }
    };
  }   // namespace detail


  template <typename SpaceObjectBase_T    = ProjectiveSpaceObject<>,
            size_t ParameterSpaceFrameDim = 1>
  using PCurve
    = ParametricObject<spaces::ParameterFVSpaceInfo<ParameterSpaceFrameDim, 1>,
                       objecttype::pcurve_tag, SpaceObjectBase_T>;

}   // namespace gmlib2::parametric

#endif   // GM2_PCURVE_H
