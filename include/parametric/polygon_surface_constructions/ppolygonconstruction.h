#ifndef GM2_PARAMETRIC_POLYGON_SURFACE_CONSTRUCTIONS_PPOLYGONCONSTRUCTION_H
#define GM2_PARAMETRIC_POLYGON_SURFACE_CONSTRUCTIONS_PPOLYGONCONSTRUCTION_H


#include "utils.h"

#include "../ppolygonsurfaceconstruction.h"
#include "../../core/utils.h"

namespace gmlib2::parametric
{
  namespace polygon_surface_constructions
  {

    template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
              size_t ParameterSpaceFrameDim_T = 2>
    class PPolygon
      : public PPolygonSurfaceConstruction<SpaceObjectEmbedBase_T,
                                           ParameterSpaceFrameDim_T> {
      using Base = PPolygonSurfaceConstruction<SpaceObjectEmbedBase_T,
                                               ParameterSpaceFrameDim_T>;

    public:
      // Dimensions
      static constexpr auto VectorDim  = Base::VectorDim;
      static constexpr auto PVectorDim = Base::PSpace_VectorDim;

      // Inherited Types
      using Unit_Type        = typename Base::Unit_Type;
      using EvaluationResult = typename Base::EvaluationResult;
      using Point            = typename Base::Point_Type;
      using Vector           = typename Base::Vector_Type;
      using VectorH          = typename Base::VectorH_Type;

      // Inherited Parameter space types
      using PSpacePoint = typename Base::PSpace_Point_Type;
      using PSizeArray  = typename Base::PSpace_SizeArray_Type;
      using PBoolArray  = typename Base::PSpace_BoolArray_Type;

      // Polygon construction types
      using Polygon2D       = typename Base::Polygon2D;
      using VertexContainer = DVectorT<Point>;

      // Constructor(s)
      template <typename... Ts>
      PPolygon(const VertexContainer& vertices, Ts&&... ts)
        : Base(vertices.size(), std::forward<Ts>(ts)...), m_vertices{vertices}
      {
      }

      const VertexContainer& vertices() const { return m_vertices; }
      size_t                 sides() const { return m_vertices.size(); }

      // Member(s)
    private:
      VertexContainer m_vertices;

      // PPolygon interface
    protected:
      PBoolArray isClosed() const override
      {
        return utils::initStaticContainer<PBoolArray, PVectorDim>(false);
      }

      PSpacePoint startParameters() const override
      {

        return utils::initStaticContainer<PSpacePoint, PVectorDim>(0.0);
      }

      PSpacePoint endParameters() const override
      {
        return utils::initStaticContainer<PSpacePoint, PVectorDim>(1.0);
      }

      EvaluationResult
      evaluate(const PSpacePoint&                 par,
               [[maybe_unused]] const PSizeArray& no_der,
               [[maybe_unused]] const PBoolArray& from_left
               = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
        const override
      {
        const auto x   = par;
        const auto mvc = utils::parametric::psc::gbc::mvc(this->polygon2D(), x);
        const auto verts = vertices();

        EvaluationResult p(1);
        blaze::subvector<0UL,VectorDim>(p[0]) = blaze::inner(mvc, verts);
        return p;
      }
    };


  }   // namespace polygon_surface_constructions

}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_POLYGON_SURFACE_CONSTRUCTIONS_PPOLYGONCONSTRUCTION_H
