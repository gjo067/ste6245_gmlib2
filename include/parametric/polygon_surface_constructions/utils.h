#ifndef GM2_PARAMETRIC_POLYGON_SURFACE_CONSTRUCTIONS_UTILS_H
#define GM2_PARAMETRIC_POLYGON_SURFACE_CONSTRUCTIONS_UTILS_H


#include "../../core/gm2_blaze.h"


namespace gmlib2::utils::parametric::psc
{

  namespace detail
  {
  }   // namespace detail


  template <typename Unit_Type_T       = double,
            typename PSpaceUnit_Type_T = Unit_Type_T>
  inline auto generateRegularPolygon2D(size_t N)
  {
    using Point2D   = VectorT<Unit_Type_T, 2>;
    using Polygon2D = DVectorT<Point2D>;

    // Construct PSpace mapping polygon
    const auto dt = (PSpaceUnit_Type_T(1) / PSpaceUnit_Type_T(N))
                    * PSpaceUnit_Type_T(2 * M_PI);

    auto circleEval2D = [](const auto t) {
      return std::forward<Point2D>(Point2D{std::cos(t), std::sin(t)});
    };

    Polygon2D P(N);
    for (size_t i = 0; i < N; ++i)
      P[i] = circleEval2D(PSpaceUnit_Type_T(i) * dt);

    using ReturnType = const Polygon2D;
    return ReturnType{P};
  }

  template <typename Unit_Type_T       = double,
            typename PSpaceUnit_Type_T = Unit_Type_T>
  inline auto generateRegularPolygon2DXZ(size_t N)
  {
    using Point3D   = VectorT<Unit_Type_T, 3>;
    using Polygon3D = DVectorT<Point3D>;

    const auto p2d
      = generateRegularPolygon2D<Unit_Type_T, PSpaceUnit_Type_T>(N);

    Polygon3D P(N);
    for (size_t i = 0; i < N; ++i)
      P[i] = Point3D{p2d[i][0], Unit_Type_T{0}, p2d[i][1]};


    using ReturnType = const Polygon3D;
    return ReturnType{P};
  }




  namespace gbc
  {



    /**!
     * Mean value coordiantes for Arbitrary Planar Polygons, Hormann and Floater
     * -- Only for simple polygons (does not contain other polygons) --
     *
     * \tparam UnitType_T UnitType - real type: (float,double)
     * \tparam Point_T Polygon Point type - euclidean type
     * \tparam VD_T Dimension of the Vector-space of the planar polygon
     *
     * \param[in] V An arbitrary M-sided Planar Polygon of
     * \param[in] x A vertex point inside the polygon
     * \param[in] F Per vertex; v in V, associated data points
     *
     * \returns the mean value weights lambda_i, i=0,...,n-1
     */
    template <typename Unit_Type_T, size_t VectorDim_T, typename FElem_T>
    inline auto mvc(const DVectorT<VectorT<Unit_Type_T, VectorDim_T>>& V,
                    const VectorT<Unit_Type_T, VectorDim_T>&           x,
                    const DVectorT<FElem_T>&                           F)
    {

      // Dimensions must be at least 3
      assert(V.size() >= 3);
      const auto M = V.size();

      // Dimensions must match
      assert(M == F.size());



      using UnitType = Unit_Type_T;
      using Vector   = VectorT<UnitType, VectorDim_T>;

      const auto N = int(M);   // PVectorDim
      //      using PSpacePoint = VectorT<UnitType, VectorDimF_T>;
      using ReturnType = const FElem_T;


      auto s = DVectorT<Vector>(size_t(N));
      std::transform(std::begin(V), std::end(V), std::begin(s),
                     [&x](const auto& v_i) { return v_i - x; });

      auto r = DVectorT<UnitType>(size_t(N));
      auto A = DVectorT<UnitType>(size_t(N));
      auto D = DVectorT<UnitType>(size_t(N));
      for (int i = 0; i < N; ++i) {

        const auto ip1 = (i + N + 1) % N;
        r[size_t(i)]   = blaze::l2Norm(s[size_t(i)]);

        MatrixT<UnitType, 2, 2> A_mat;
        blaze::column(A_mat, 0UL) = s[size_t(i)];
        blaze::column(A_mat, 1UL) = s[size_t(ip1)];

        A[size_t(i)] = blaze::det(A_mat) / UnitType(2);
        D[size_t(i)] = blaze::inner(s[size_t(i)], s[size_t(ip1)]);

        // x == v_i
        if (std::abs(r[size_t(i)]) < 1e-7) return ReturnType(F[size_t(i)]);

        // x \in e_i (v_i,v_{i+1})
        if (std::abs(A[size_t(i)]) < 1e-7 and D[size_t(i)] < 0) {

          r[size_t(ip1)] = blaze::l2Norm(s[size_t(ip1)]);

          return ReturnType(
            ((r[size_t(ip1)] * F[size_t(i)]) + (r[size_t(i)] * F[size_t(ip1)]))
            / (r[size_t(i)] + r[size_t(ip1)]));
        }
      }

      FElem_T  f(M, 0);
      UnitType W = 0;
      for (int i = 0; i < N; ++i) {

        const auto ip1 = (i + N + 1) % N;
        const auto im1 = (i + N - 1) % N;

        UnitType w = 0;

        if (std::abs(A[size_t(im1)]) >= 1e-7)
          w += (r[size_t(im1)] - (D[size_t(im1)] / r[size_t(i)]))
               / A[size_t(im1)];

        if (std::abs(A[size_t(i)]) >= 1e-7)
          w += (r[size_t(ip1)] - (D[size_t(i)] / r[size_t(i)])) / A[size_t(i)];

        f += w * F[size_t(i)];
        W += w;
      }

      const auto f_over_W = f / W;

      return ReturnType(f_over_W);
    }


    template <typename Unit_Type_T, size_t VectorDim_T>
    inline auto mvc(const DVectorT<VectorT<Unit_Type_T, VectorDim_T>>& V,
                    const VectorT<Unit_Type_T, VectorDim_T>&           x)
    {

      // Dimensions must be at least 3
      assert(V.size() >= 3);
      const auto M = V.size();


      using PSpacePoint = DVectorT<Unit_Type_T>;

      auto F = DVectorT<PSpacePoint>(M, PSpacePoint(M, Unit_Type_T{0}));
      for (size_t i = 0; i < M; ++i) F[i][i] = Unit_Type_T{1};

      return mvc(V, x, F);
    }





    template <typename Unit_Type_T>
    inline auto gbcToSMapping(const DVectorT<VectorT<Unit_Type_T, 2>>& V,
                              const DVectorT<Unit_Type_T>& lambda)
    {
      // Polygon sides
      const auto N = V.size();
      assert(N == lambda.size());

      // Rotate polygon index
      auto rotate_idx = [N](int k) { return size_t((int(N) + k) % int(N)); };

      // Angle of Vector [jm,j,jp] in V
      auto compute_theta
        = [](const auto& vjm, const auto& vj, const auto& vjp) {
            const auto a = vjm - vj;
            const auto b = vjp - vj;

            const auto co
              = blaze::inner(a, b) / (blaze::l2Norm(a) * blaze::l2Norm(b));
            return blaze::acos(co);
          };

      // Compute the perpendicular distance from v onto the line [vi,vim1]
      auto compute_perp_dist = [](const auto& vi, const auto& vim1,
                                  const auto& v) {
        const auto vj = vi;
        const auto vjm1
          = blaze::l2Norm(v - vim1) >= 1e-5 ? vim1 : vim1 - (0.1 * (vi - vim1));

        const auto a = vj - vjm1;
        const auto b = v - vjm1;

        const auto c
          = (a * blaze::inner(a, b)) / (blaze::length(a) * blaze::length(a));
        //          const auto c
        //            = (a * blaze::inner(a, b)) / (blaze::length(a) *
        //            blaze::length(a));

        const auto d = b - c;

        const auto l2N_d = blaze::l2Norm(d);

        //          std::cout << "a:\n"
        //                    << a << ", b:\n"
        //                    << b << ", c:\n"
        //                    << c << ", d:\n"
        //                    << d << ", ||d||: " << l2N_d << std::endl;

        return l2N_d;
      };


      // Indices
      const auto im2 = rotate_idx(-2);
      const auto im1 = rotate_idx(-1);
      const auto i   = rotate_idx(0);
      const auto ip1 = rotate_idx(1);

      // Vertices
      const auto v    = blaze::inner(V, lambda);
      const auto vim2 = V.at(im2);
      const auto vim1 = V.at(im1);
      const auto vi   = V.at(i);
      const auto vip1 = V.at(ip1);
      //    std::cout << "v:\n"
      //              << v << ", vim2:\n"
      //              << vim2 << ", vim1:\n"
      //              << vim1 << ", vi:\n"
      //              << vi << ", vip1:\n"
      //              << vip1 << std::endl;

      // compute side-coordinate (s or u)
      const auto stim = std::sin(compute_theta(vim2, vim1, vi));
      const auto sti  = std::sin(compute_theta(vim1, vi, vip1));

      //    std::cout << "------------------- == him" << std::endl;
      const auto him = compute_perp_dist(vim2, vim1, v);

      //    std::cout << "------------------- == hip" << std::endl;
      const auto hip = compute_perp_dist(vip1, vi, v);

      //    std::cout << "------------------ him: " << him << std::endl;
      //    std::cout << "------------------ hip: " << hip << std::endl;

      const auto s = (sti * him) / ((sti * him) + (stim * hip));



      //    std::cout << " gbcToSMapping:";
      //    std::cout << ", lambda: ";
      //    for( const auto& l : lambda ) std::cout << l << " ";
      //    std::cout << ", im2: " << im2 << ", im1: " << im1 << ", i: " << i
      //              << ", ip1: " << ip1;
      //    std::cout << ", s: " << s;
      //    std::cout << std::endl;


      return s;
    }



    template <typename Unit_Type_T>
    inline auto gbcToHMapping(const DVectorT<Unit_Type_T>& lambda)
    {
      // Polygon sides
      const auto N = lambda.size();

      // Rotate polygon index
      auto rotate_idx = [N](int k) { return size_t((int(N) + k) % int(N)); };

      // Indices
      const auto im = rotate_idx(-1);
      const auto i  = rotate_idx(0);

      // compute distance-coordinate (h or v)
      const auto h = Unit_Type_T(1) - lambda[im] - lambda[i];

      //    std::cout << " gbcToHMapping:";
      //    std::cout << ", lambda: ";
      //    for( const auto& l : lambda ) std::cout << l << " ";
      //    std::cout << ", idxm: " << im << ", idx: " << i;
      //    std::cout << ", h: " << h;
      //    std::cout << std::endl;

      return h;
    }


  }   // namespace gbc



}   // namespace gmlib2::utils::parametric::psc









#endif   // GM2_PARAMETRIC_POLYGON_SURFACE_CONSTRUCTIONS_UTILS_H
