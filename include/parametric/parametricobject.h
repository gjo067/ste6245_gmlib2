#ifndef GM2_PARAMETRICSPACEOBJECT_H
#define GM2_PARAMETRICSPACEOBJECT_H

// gmlib2
#include "../core/space.h"
#include "../core/projectivespaceobject.h"
#include "../core/utils.h"

// stl
#include <array>

namespace gmlib2::spaces
{

  template <size_t F, size_t V, typename Unit_T = double>
  struct ParameterFVSpaceInfo {
    using Unit_Type = Unit_T;
    static constexpr auto FrameDim  = F;
    static constexpr auto VectorDim = V;
  };

  template <size_t V, typename Unit_T = double>
  struct ParameterVSpaceInfo : ParameterFVSpaceInfo<V, V, Unit_T> {
  };

}   // namespace gmlib2::spaces

namespace gmlib2::parametric
{
  namespace objecttype
  {

    // clang-format off
    struct pvolume_tag { };
    struct ppolygonsurface_tag { };
    // clang-format on

  }   // namespace objecttype









  namespace detail
  {
    template <typename ParametricSpaceInfo_T,
              typename ParametricObjectType_Tag_T,
              typename SpaceObjectEmbedBase_T, typename Enable = void>
    class Sampler;





    /////////////////////////////////////////////
    /// Default Parametric PolygonSurface Sampler
    ///
    template <typename ParametricSpaceInfo_T, typename SpaceObjectEmbedBase_T>
    class Sampler<ParametricSpaceInfo_T, objecttype::ppolygonsurface_tag,
                  SpaceObjectEmbedBase_T> {
    public:
      using ParameterSpaceInfo = ParametricSpaceInfo_T;
      using ParameterSpace     = spaces::ProjectiveSpace<ParametricSpaceInfo_T>;
      static constexpr size_t PSpace_VectorDim = ParameterSpace::VectorDim;
      using PSpacePoint = typename ParameterSpace::Point_Type;
      using PSizeArray  = std::array<size_t, PSpace_VectorDim>;
      using PBoolArray  = std::array<bool, PSpace_VectorDim>;


      using EvaluationResult
        = DVectorT<typename SpaceObjectEmbedBase_T::VectorH_Type>;
      using SampleResult = DVectorT<EvaluationResult>;

      template <typename Object_T>
      static SampleResult sample(Object_T* pobj, const PSpacePoint& par_start,
                                 const PSpacePoint& /*par_end*/,
                                 size_t samples_M)
      {

        constexpr auto no_der
          = utils::initStaticContainer<PSizeArray, PSpace_VectorDim>(0UL);
        constexpr auto from_left
          = utils::initStaticContainer<PBoolArray, PSpace_VectorDim>(true);


        static constexpr int N = int(PSpace_VectorDim);
        int                  M = int(samples_M);


        size_t no_samples = 0;   // M == 0;
        if (M == 1)
          no_samples = 1;
        else {
          no_samples = 1;
          for (int k = 2; k <= M; ++k) {
            no_samples += N * (k - 1);
          }
        }

        SampleResult p(no_samples);

        const auto center
          = utils::initStaticContainer<PSpacePoint, PSpace_VectorDim>(
            1.0 / typename ParameterSpace::Unit_Type{N});

        // Sample in each of the control point vertices
        auto par = VectorT<PSpacePoint, N>{par_start};
        for (size_t i = 0; i < N; ++i) {
          par[i][i] = 1.0;
        }



        p[0] = pobj->evaluateLocal(center, no_der, from_left);


        //          std::cout << "Polygon sampling; no. samples: " << no_samples
        //          << std::endl;

        //          std::cout << "Polygon parametric vertex points; [center, 0,
        //          ..., N-1]: " << std::endl
        //                    << center << std::endl;
        //          for( const auto& par_ : par )
        //            std::cout << par_ << std::endl;

        //          std::cout << "Polygon sampling indexing for N,M: " << N <<
        //          "," << M << std::endl;
        for (int m = 0; m < M; ++m) {

          const int T = m - 1;

          int r_o;
          if (m == 0)
            r_o = 0;
          else if (m == 1)
            r_o = 1;
          else {
            r_o = 1;
            for (int k = 2; k <= m; ++k) {
              r_o += N * (k - 1);
            }
          }

          for (int n = 0; n < (m == 0 ? 1 : N); ++n) {

            const int t_o = r_o + n * T + n;



            for (int t_i = 0; t_i <= T; ++t_i) {

              const int idx = t_o + t_i;
              //              std::cout << "idx: " << idx << " ";
              //                std::cout << "  n,m: " << n << "," << m <<
              //                std::endl; std::cout << "     -> r_o: " << r_o
              //                          << ", t_o: " << t_o << ", idx: " <<
              //                          idx << std::endl;

              const double u = 1.0 - ((1.0 / double(M - 1)) * m);
              const double w = (1.0 / double(M - 1)) * t_i;
              const double v = 1.0 - (u + w);

              //                std::cout << "     -> u,v,w: "
              //                          << "(" << u << "," << v << "," << w <<
              //                          ")"
              //                          << std::endl;
              const auto poly_par = blaze::evaluate(
                center * u + par[n] * v + par[n == N - 1 ? 0 : n + 1] * w);

              //                std::cout << "     -> poly_par: " << std::endl
              //                << poly_par << std::endl;

              p[idx] = pobj->evaluateLocal(poly_par, no_der, from_left);
              //              std::cout << " p[idx]: " << p[idx] << std::endl;
            }
          }
        }

        return p;
      }
    };
  }   // namespace detail










  ////////////////////////
  /// ParametricObject ///
  ///
  template <typename ParametricSpaceInfo_T, typename ParametricObjectType_Tag_T,
            typename SpaceObjectEmbedBase_T,
            template <typename Sampler_PSI_T, typename Sampler_POT_Tag_T,
                      typename Sampler_SOEB_T, typename Sampler_Enable>
            typename Sampler_T
            = detail::Sampler>
  struct ParametricObject : SpaceObjectEmbedBase_T {
    using Base                      = SpaceObjectEmbedBase_T;
    using ParametricObjectEmbedBase = SpaceObjectEmbedBase_T;
    using ParametricObjectType_Tag  = ParametricObjectType_Tag_T;

    // Embed space
    using EmbedSpace                = typename Base::EmbedSpace;
    using Unit_Type                 = typename EmbedSpace::Unit_Type;
    static constexpr auto FrameDim  = EmbedSpace::FrameDim;
    static constexpr auto VectorDim = EmbedSpace::VectorDim;
    using Point_Type                = typename EmbedSpace::Point_Type;
    using Vector_Type               = typename EmbedSpace::Vector_Type;
    using Frame_Type                = typename EmbedSpace::Frame_Type;
    using PointH_Type               = typename EmbedSpace::PointH_Type;
    using VectorH_Type              = typename EmbedSpace::VectorH_Type;
    using ASFrameH_Type             = typename EmbedSpace::ASFrameH_Type;

    // Parameter space
    using PSpaceInfo       = ParametricSpaceInfo_T;
    using PSpace           = spaces::ProjectiveSpace<PSpaceInfo>;
    using PSpace_Unit_Type = typename PSpace::Unit_Type;
    static constexpr size_t PSpace_FrameDim  = PSpace::FrameDim;
    static constexpr size_t PSpace_VectorDim = PSpace::VectorDim;
    using PSpace_Point_Type                  = typename PSpace::Point_Type;
    using PSpace_Vector_Type                 = typename PSpace::Vector_Type;
    using PSpace_Frame_Type                  = typename PSpace::Frame_Type;
    using PSpace_PointH_Type                 = typename PSpace::PointH_Type;
    using PSpace_VectorH_Type                = typename PSpace::VectorH_Type;
    using PSpace_ASFrameH_Type               = typename PSpace::ASFrameH_Type;

    // Sampler
    using Sampler = Sampler_T<ParametricSpaceInfo_T, ParametricObjectType_Tag_T,
                              SpaceObjectEmbedBase_T, void>;

    // Data types
    using EvaluationResult = typename Sampler::EvaluationResult;
    using SampleResult     = typename Sampler::SampleResult;

    // Paramter space types
    using PSpace_SizeArray_Type = std::array<size_t, PSpace_VectorDim>;
    using PSpace_BoolArray_Type = std::array<bool, PSpace_VectorDim>;

    // Constructor(s)
    using Base::Base;

    // Members
    template <typename... Ts>
    auto sample(Ts&&... ts) const
    {
      auto par_s = startParameters();
      auto par_e = endParameters();
      return Sampler::sample(this, par_s, par_e, ts...);
    }

    EvaluationResult evaluateLocal(
      const PSpace_Point_Type&     par,
      const PSpace_SizeArray_Type& no_der
      = utils::initStaticContainer<PSpace_SizeArray_Type, PSpace_VectorDim>(
        0UL),
      const PSpace_BoolArray_Type& from_left
      = utils::initStaticContainer<PSpace_BoolArray_Type, PSpace_VectorDim>(
        true)) const;
    EvaluationResult evaluateParent(
      const PSpace_Point_Type&     par,
      const PSpace_SizeArray_Type& no_der
      = utils::initStaticContainer<PSpace_SizeArray_Type, PSpace_VectorDim>(
        0UL),
      const PSpace_BoolArray_Type& from_left
      = utils::initStaticContainer<PSpace_BoolArray_Type, PSpace_VectorDim>(
        true)) const;


    // Interface
    virtual PSpace_BoolArray_Type isClosed() const        = 0;
    virtual PSpace_Point_Type     startParameters() const = 0;
    virtual PSpace_Point_Type     endParameters() const   = 0;

//    PSpace_Point_Type constructBCPSpacePoint(size_t idx) const
//    {
//      PSpace_Point_Type par;
//      for (int i = 0; i < idx; ++i) par[i] = PSpace_Unit_Type(0);
//      par[idx] = PSpace_Unit_Type(1);
//      for (int i = idx + 1; i < PSpace_VectorDim; ++i)
//        par[i] = PSpace_Unit_Type(0);
//      return par;
//    }

//    PSpace_Point_Type constructBCCenterPSpacePoint() const
//    {
//      return PSpace_Point_Type(1.0 / double(PSpace_VectorDim));
//    }

  protected:
    virtual EvaluationResult
    evaluate(const PSpace_Point_Type& par, const PSpace_SizeArray_Type& no_der,
             const PSpace_BoolArray_Type& from_left) const = 0;
  };




  //  template <typename ParameterSpaceInfo_T, typename
  //  ParametricObjectType_Tag_T,
  //            typename SpaceObjectEmbedBase_T,
  //            template <typename Sampler_PSI_T, typename Sampler_POT_Tag_T,
  //                      typename Sampler_SOEB_T, typename Sampler_Enable>
  //            typename Sampler_T>
  //  template <typename... Ts>
  //  auto ParametricObject<
  //    ParameterSpaceInfo_T, ParametricObjectType_Tag_T,
  //    SpaceObjectEmbedBase_T, Sampler_T>::sample(Ts&&... ts) const
  //  {
  //    auto par_s = startParameters();
  //    auto par_e = endParameters();
  //    return Sampler::sample(this, par_s, par_e, ts...);
  //  }

  template <typename ParameterSpaceInfo_T, typename ParametricObjectType_Tag_T,
            typename SpaceObjectEmbedBase_T,
            template <typename Sampler_PSI_T, typename Sampler_POT_Tag_T,
                      typename Sampler_SOEB_T, typename Sampler_Enable>
            typename Sampler_T>
  typename ParametricObject<ParameterSpaceInfo_T, ParametricObjectType_Tag_T,
                            SpaceObjectEmbedBase_T, Sampler_T>::EvaluationResult
  ParametricObject<
    ParameterSpaceInfo_T, ParametricObjectType_Tag_T, SpaceObjectEmbedBase_T,
    Sampler_T>::evaluateLocal(const PSpace_Point_Type&     par,
                              const PSpace_SizeArray_Type& no_der,
                              const PSpace_BoolArray_Type& from_left) const
  {
    return evaluate(par, no_der, from_left);
  }

  template <typename ParameterSpaceInfo_T, typename ParametricObjectType_Tag_T,
            typename SpaceObjectEmbedBase_T,
            template <typename Sampler_PSI_T, typename Sampler_POT_Tag_T,
                      typename Sampler_SOEB_T, typename Sampler_Enable>
            typename Sampler_T>
  typename ParametricObject<ParameterSpaceInfo_T, ParametricObjectType_Tag_T,
                            SpaceObjectEmbedBase_T, Sampler_T>::EvaluationResult
  ParametricObject<
    ParameterSpaceInfo_T, ParametricObjectType_Tag_T, SpaceObjectEmbedBase_T,
    Sampler_T>::evaluateParent(const PSpace_Point_Type&     par,
                               const PSpace_SizeArray_Type& no_derivatives,
                               const PSpace_BoolArray_Type& from_left) const
  {
    auto eval_res = evaluate(par, no_derivatives, from_left);
    auto pframe   = this->pSpaceFrameParent();

    EvaluationResult res = blaze::map(eval_res, [&pframe](const auto& ele) {
      return blaze::evaluate(pframe * ele);
    });
    return res;
    //    return evaluate(par, no_der, from_left);

    //    auto hframe = blaze::eval(this->hFrame());
    //    return blaze::eval(
    //      blaze::map(evaluate(par, no_der, from_left),
    //                 [&hframe](auto ele) { return hframe * ele; }));
  }










  ///////////////////
  /// PPolygonSurface
  template <size_t ParameterSpaceVectorDim_T,
            typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            size_t ParameterSpaceFrameDim_T = ParameterSpaceVectorDim_T>
  using PPolygonSurface
    = ParametricObject<spaces::ParameterFVSpaceInfo<ParameterSpaceFrameDim_T,
                                                    ParameterSpaceVectorDim_T>,
                       objecttype::ppolygonsurface_tag, SpaceObjectEmbedBase_T>;


}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRICSPACEOBJECT_H
