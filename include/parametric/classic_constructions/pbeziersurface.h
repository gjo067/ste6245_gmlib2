#ifndef GM2_PARAMETRIC_SURFACES_PBEZIERSURFACE_H
#define GM2_PARAMETRIC_SURFACES_PBEZIERSURFACE_H

#include <gmlib2.h>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PBezierSurface
    : public PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    using ControlNet = DMatrixT<Point>;

    // Constructor(s)
    template <typename... Ts>
    PBezierSurface(const ControlNet& C, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C{C}
    {
      //      std::cout << "m_C: " << m_C << std::endl;
    }

    // Members
    ControlNet m_C;

    // TPSurf interface
  public:
    PBoolArray  isClosed() const final { return {{false, false}}; }
    PSpacePoint startParameters() const final { return PSpacePoint{0, 0}; }
    PSpacePoint endParameters() const final { return PSpacePoint{1, 1}; }

  protected:
    EvaluationResult evaluate(
      const PSpacePoint& par, const PSizeArray& no_der,
      const PBoolArray& /*from_left*/
      = utils::initStaticContainer<PBoolArray, PVectorDim>(true)) const final
    {

      const auto u    = par[0];
      const auto v    = par[1];
      const auto nd_u = std::min(no_der[0], m_C.rows() - 1);
      const auto nd_v = std::min(no_der[1], m_C.columns() - 1);

      EvaluationResult p(nd_u + 1, nd_v + 1);


      const auto Bu
        = gmlib2::basis::generateBernsteinBasisMatrix(int(m_C.rows() - 1), u);
      const auto Bv = gmlib2::basis::generateBernsteinBasisMatrix(
        int(m_C.columns() - 1), v);


      const auto res = Bu * (m_C * blaze::trans(Bv));

      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        res(0, 0), 1.0);

      if (nd_u)
        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(1, 0), 0.0);

      if (nd_v)
        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(0, 1), 0.0);

      if (nd_u and nd_v)
        p(1, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(1, 1), 0.0);

      using ReturnType = const EvaluationResult;
      return ReturnType(p);
    }
  };





}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SURFACES_PBEZIERSURFACE_H
