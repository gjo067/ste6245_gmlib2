#ifndef GM2_PARAMETRIC_CURVES_PBEZIERCURVE_H
#define GM2_PARAMETRIC_CURVES_PBEZIERCURVE_H

#include "../pcurve.h"
#include "../../core/utils.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 1>
  class PBezierCurve
    : public PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    using ControlPoints = DColVectorT<Point>;

    // Constructor
    template <typename... Ts>
    PBezierCurve(const ControlPoints& C, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C{C}
    {
    }

    // Members
    ControlPoints m_C;


    // PCurve interface
  public:
    PBoolArray  isClosed() const override {
      return {{false}};
    }
    PSpacePoint startParameters() const override {
      return PSpacePoint{0};
    }
    PSpacePoint endParameters() const override {
      return PSpacePoint{1};
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& /*from_left*/
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
    const override
    {
      const auto& t              = par[0];
      const auto& no_derivatives = no_der[0];

      EvaluationResult p(no_derivatives + 1);

      const auto B = gmlib2::basis::generateBernsteinBasisMatrix(int(m_C.size()-1),t);


      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        blaze::row(B, 0) * m_C, 1.0);
      if (no_derivatives)
        p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          blaze::row(B, 1) * m_C, 0.0);
      if (no_derivatives > 1)
        p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          blaze::row(B, 2) * m_C, 0.0);
      if (no_derivatives > 2)
        p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          blaze::row(B, 3) * m_C, 0.0);

      std::cout << "p:\n" << p << std::endl;
      return p;
    }
  };



}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_CURVES_PBEZIERCURVE_H
