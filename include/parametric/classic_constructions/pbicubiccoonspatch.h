#ifndef GM2_PBICUBICCOONSPATCH_H
#define GM2_PBICUBICCOONSPATCH_H

#include "../classic_constructions/phermitecurve.h"

#include "../parametricobject.h"
#include "../../core/utils.h"

// stl
#include <array>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PBicubicCoonsPatch
    : public PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    static constexpr auto VectorDim = Base::VectorDim;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor(s)
    template <typename... Ts>
    PBicubicCoonsPatch(PCurve<SpaceObjectEmbedBase_T>* c0,
                       PCurve<SpaceObjectEmbedBase_T>* c1,
                       PCurve<SpaceObjectEmbedBase_T>* c2,
                       PCurve<SpaceObjectEmbedBase_T>* c3,
                       PCurve<SpaceObjectEmbedBase_T>* g0,
                       PCurve<SpaceObjectEmbedBase_T>* g1,
                       PCurve<SpaceObjectEmbedBase_T>* g2,
                       PCurve<SpaceObjectEmbedBase_T>* g3, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c{{c0, c1, c2, c3}}, m_g{{g0, g1, g2,
                                                                   g3}}
    {
    }

    // Members
    std::array<PCurve<SpaceObjectEmbedBase_T>*, 4> m_c;
    std::array<PCurve<SpaceObjectEmbedBase_T>*, 4> m_g;

    bool validateConstruction() const;

  private:
    // TPSurf interface
  public:
    PBoolArray  isClosed() const final;
    PSpacePoint startParameters() const final;
    PSpacePoint endParameters() const final;

  protected:
    EvaluationResult evaluate(
      const PSpacePoint& par, const PSizeArray& no_der,
      const PBoolArray& from_left
      = utils::initStaticContainer<PBoolArray, PVectorDim>(true)) const final;
  };







  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  bool
  PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                     ParametricSpaceFrameDim_T>::validateConstruction() const
  {
    auto evalP_start = [](auto* c) {
      return c->evaluateParent(c->startParameters(), {{0}})[0];
    };

    auto evalP_end
      = [](auto* c) { return c->evaluateParent(c->endParameters(), {{0}})[0]; };

    auto evalD1_start = [](auto* c) {
      return c->evaluateParent(c->startParameters(), {{0}})[1];
    };

    auto evalD1_end
      = [](auto* c) { return c->evaluateParent(c->endParameters(), {{0}})[1]; };

    auto checkError
      = [](auto p0, auto p1) { return blaze::length(p0 - p1) < 1e-7; };

    auto c0s  = evalP_start(m_c[0]);
    auto c0e  = evalP_end(m_c[0]);
    auto c1s  = evalP_start(m_c[1]);
    auto c1e  = evalP_end(m_c[1]);
    auto Dc0s = evalD1_start(m_c[0]);
    auto Dc0e = evalD1_end(m_c[0]);
    auto Dc1s = evalD1_start(m_c[1]);
    auto Dc1e = evalD1_end(m_c[1]);
    auto c2s  = evalP_start(m_c[2]);
    auto c2e  = evalP_end(m_c[2]);
    auto c3s  = evalP_start(m_c[3]);
    auto c3e  = evalP_end(m_c[3]);
    auto Dc2s = evalD1_start(m_c[2]);
    auto Dc2e = evalD1_end(m_c[2]);
    auto Dc3s = evalD1_start(m_c[3]);
    auto Dc3e = evalD1_end(m_c[3]);
    auto g0s  = evalP_start(m_g[0]);
    auto g0e  = evalP_end(m_g[0]);
    auto g1s  = evalP_start(m_g[1]);
    auto g1e  = evalP_end(m_g[1]);
    auto Dg0s = evalD1_start(m_g[0]);
    auto Dg0e = evalD1_end(m_g[0]);
    auto Dg1s = evalD1_start(m_g[1]);
    auto Dg1e = evalD1_end(m_g[1]);
    auto g2s  = evalP_start(m_g[2]);
    auto g2e  = evalP_end(m_g[2]);
    auto g3s  = evalP_start(m_g[3]);
    auto g3e  = evalP_end(m_g[3]);
    auto Dg2s = evalD1_start(m_g[2]);
    auto Dg2e = evalD1_end(m_g[2]);
    auto Dg3s = evalD1_start(m_g[3]);
    auto Dg3e = evalD1_end(m_g[3]);

    // clang-format off
    return     checkError(c0s,  g0s) and checkError(c0e,  g1s)
           and checkError(c1s,  g0e) and checkError(c1e,  g1e)
           and checkError(Dc0s, g2s) and checkError(Dc0e, g3s)
           and checkError(Dc1s, g2e) and checkError(Dc1e, g3e)
           and checkError(Dg0s, c2s) and checkError(Dg0e, c3s)
           and checkError(Dg1s, c2e) and checkError(Dg1e, c3e)

           and checkError(Dc2s,Dg2s) and checkError(Dc3s,Dg2e)
           and checkError(Dc2e,Dg3s) and checkError(Dc3e,Dg3e);
    // clang-format on
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                              ParametricSpaceFrameDim_T>::PBoolArray
  PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                     ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false, false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                              ParametricSpaceFrameDim_T>::PSpacePoint
  PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                     ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                              ParametricSpaceFrameDim_T>::PSpacePoint
  PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                     ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{1, 1};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBicubicCoonsPatch<SpaceObjectEmbedBase_T,
                              ParametricSpaceFrameDim_T>::EvaluationResult
  PBicubicCoonsPatch<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left) const
  {
    // Evalaute curve at start/end
    auto evalC = [](auto* c, auto nd, auto fl, auto at_start, auto component) {
      if (at_start)
        return blaze::evaluate(blaze::subvector(
          c->evaluateParent(c->startParameters(), {{nd}}, {{fl}})[component],
          0UL, VectorDim));
      else
        return blaze::evaluate(blaze::subvector(
          c->evaluateParent(c->endParameters(), {{nd}}, {{fl}})[component], 0UL,
          VectorDim));
    };

    // Evalaute mapped curve at
    auto evalMC = [](auto* c, auto t, auto t_delta, auto nd_t, auto fl_t,
                     auto component) {
      auto c_s     = c->startParameters();
      auto c_e     = c->endParameters();
      auto c_d     = c_e - c_s;
      auto c_t_map = t * (c_d / t_delta);
      return blaze::evaluate(blaze::subvector(
        c->evaluateParent(c_t_map, {{nd_t}}, {{fl_t}})[component], 0UL,
        VectorDim));
    };

    // Parameter extraction
    const auto u    = par[0];
    const auto v    = par[1];
    const auto nd_u = no_der[0];
    const auto nd_v = no_der[1];
    const auto fl_u = from_left[0];
    const auto fl_v = from_left[1];

    EvaluationResult p(nd_u + 1, nd_v + 1);

    auto par_delta = endParameters() - startParameters();
    auto u_delta   = par_delta[0];
    auto v_delta   = par_delta[1];


    // Compute hermite basis for u and v
    auto Hu   = basis::generateHermiteBasisVectorF2D2<Unit_Type, 0,
                                                    blaze::columnVector>(u);
    auto Hv   = basis::generateHermiteBasisVectorF2D2<Unit_Type, 0>(v);
    auto DuHu = basis::generateHermiteBasisVectorF2D2<Unit_Type, 1,
                                                      blaze::columnVector>(u);
    auto DvHv = basis::generateHermiteBasisVectorF2D2<Unit_Type, 1>(v);

    // Compute evaluation vector from C curves
    RowVectorT<Vector, 4> cV{evalMC(m_c[0], v, v_delta, nd_v, fl_v, 0UL),
                             evalMC(m_c[1], v, v_delta, nd_v, fl_v, 0UL),
                             evalMC(m_c[2], v, v_delta, nd_v, fl_v, 0UL),
                             evalMC(m_c[3], v, v_delta, nd_v, fl_v, 0UL)};
    RowVectorT<Vector, 4> DvcV{evalMC(m_c[0], v, v_delta, nd_v, fl_v, 1UL),
                               evalMC(m_c[1], v, v_delta, nd_v, fl_v, 1UL),
                               evalMC(m_c[2], v, v_delta, nd_v, fl_v, 1UL),
                               evalMC(m_c[3], v, v_delta, nd_v, fl_v, 1UL)};

    // Compute evaluation vector from G curves
    ColVectorT<Vector, 4> gU{evalMC(m_g[0], u, u_delta, nd_u, fl_u, 0UL),
                             evalMC(m_g[1], u, u_delta, nd_u, fl_u, 0UL),
                             evalMC(m_g[2], u, u_delta, nd_u, fl_u, 0UL),
                             evalMC(m_g[3], u, u_delta, nd_u, fl_u, 0UL)};
    ColVectorT<Vector, 4> DugU{evalMC(m_g[0], u, u_delta, nd_u, fl_u, 1UL),
                               evalMC(m_g[1], u, u_delta, nd_u, fl_u, 1UL),
                               evalMC(m_g[2], u, u_delta, nd_u, fl_u, 1UL),
                               evalMC(m_g[3], u, u_delta, nd_u, fl_u, 1UL)};

    // Compute evaluation-correction-matrix from C curves
    // clang-format off
    auto c0_0  = evalC(m_c[0], nd_v, fl_v, true,  0UL);
    auto c1_0  = evalC(m_c[1], nd_v, fl_v, true,  0UL);
    auto c2_0  = evalC(m_c[2], nd_v, fl_v, true,  0UL);
    auto c3_0  = evalC(m_c[3], nd_v, fl_v, true,  0UL);

    auto c0_1  = evalC(m_c[0], nd_v, fl_v, false, 0UL);
    auto c1_1  = evalC(m_c[1], nd_v, fl_v, false, 0UL);
    auto c2_1  = evalC(m_c[2], nd_v, fl_v, false, 0UL);
    auto c3_1  = evalC(m_c[3], nd_v, fl_v, false, 0UL);

    auto Dc0_0 = evalC(m_c[0], nd_v, fl_v, true,  1UL);
    auto Dc1_0 = evalC(m_c[1], nd_v, fl_v, true,  1UL);
    auto Dc2_0 = evalC(m_c[2], nd_v, fl_v, true,  1UL);
    auto Dc3_0 = evalC(m_c[3], nd_v, fl_v, true,  1UL);

    auto Dc0_1 = evalC(m_c[0], nd_v, fl_v, false, 1UL);
    auto Dc1_1 = evalC(m_c[1], nd_v, fl_v, false, 1UL);
    auto Dc2_1 = evalC(m_c[2], nd_v, fl_v, false, 1UL);
    auto Dc3_1 = evalC(m_c[3], nd_v, fl_v, false, 1UL);

    auto cM = SqMatrixT<Vector, 4> {
      { c0_0,  c1_0,  c2_0,  c3_0},
      { c0_1,  c1_1,  c2_1,  c3_1},
      {Dc0_0, Dc1_0, Dc2_0, Dc3_0},
      {Dc0_1, Dc1_1, Dc2_1, Dc3_1}
    };

//    auto g0_0  = evalC(m_g[0], nd_v, fl_v, true,  0UL);
//    auto g1_0  = evalC(m_g[1], nd_v, fl_v, true,  0UL);
//    auto g2_0  = evalC(m_g[2], nd_v, fl_v, true,  0UL);
//    auto g3_0  = evalC(m_g[3], nd_v, fl_v, true,  0UL);

//    auto g0_1  = evalC(m_g[0], nd_v, fl_v, false, 0UL);
//    auto g1_1  = evalC(m_g[1], nd_v, fl_v, false, 0UL);
//    auto g2_1  = evalC(m_g[2], nd_v, fl_v, false, 0UL);
//    auto g3_1  = evalC(m_g[3], nd_v, fl_v, false, 0UL);

//    auto Dg0_0 = evalC(m_g[0], nd_v, fl_v, true,  1UL);
//    auto Dg1_0 = evalC(m_g[1], nd_v, fl_v, true,  1UL);
//    auto Dg2_0 = evalC(m_g[2], nd_v, fl_v, true,  1UL);
//    auto Dg3_0 = evalC(m_g[3], nd_v, fl_v, true,  1UL);

//    auto Dg0_1 = evalC(m_g[0], nd_v, fl_v, false, 1UL);
//    auto Dg1_1 = evalC(m_g[1], nd_v, fl_v, false, 1UL);
//    auto Dg2_1 = evalC(m_g[2], nd_v, fl_v, false, 1UL);
//    auto Dg3_1 = evalC(m_g[3], nd_v, fl_v, false, 1UL);

//    auto gM = SqMatrixT<Vector, 4> {
//      { g0_0,  g0_1, Dg0_0, Dg0_1},
//      { g1_0,  g1_1, Dg1_0, Dg1_1},
//      { g2_0,  g2_1, Dg2_0, Dg2_1},
//      { g3_0,  g3_1, Dg3_0, Dg3_1}
//    };

//    auto zvec = Vector{0.0,0.0,0.0};
//    auto xM = SqMatrixT<Vector, 4> {
//      { c0_0,  c1_0,  c2_0,  c3_0},
//      { c0_1,  c1_1,  c2_1,  c3_1},
//      { g2_0,  g2_1,  zvec,  zvec},
//      { g3_0,  g3_1,  zvec,  zvec}
//    };


    auto M = cM;
    // clang-format on

    // Compute bicubic interpolation
    auto S1 = cV * Hu;
    auto S2 = Hv * gU;
    auto S3 = Hv * M * Hu;

    auto S = S1 + S2 - S3;
    p(0, 0)
      = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(S, 1.0);








    /// DERIVATIVES

    // DuS
    if (nd_u) {
      auto DuS1 = /* DucV * Hu + */ cV * DuHu;
      auto DuS2 = /* DuHv * gV + */ Hv * DugU;
      auto DuS3 = /* DuHv * M * Hu + Hv * DuM * Hu + */ Hv * M * DuHu;
      auto DuS  = DuS1 + DuS2 - DuS3;
      p(1, 0)   = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        DuS, 0.0);
    }

    // DvS : DvcM = DvgM = 0
    if (nd_v) {
      auto DvS1 = DvcV * Hu;     /* + cV * DvHu */
      auto DvS2 = DvHv * gU;     /* + Hv * DvgU */
      auto DvS3 = DvHv * M * Hu; /* + Hv * DvM * Hu + Hv * M * DvHu */
      auto DvS  = DvS1 + DvS2 - DvS3;
      p(0, 1)   = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        DvS, 0.0);
      //      p(0, 1)    {DvS[0], DvS[1], DvS[2], 0.0};
    }

    // DvDuS = DuDvS
    if (nd_u and nd_v) {
      auto DvDuS1 = DvcV * DuHu;
      auto DvDuS2 = DvHv * DugU;
      auto DvDuS3 = DvHv * M * DuHu;
      auto DvDuS  = DvDuS1 + DvDuS2 - DvDuS3;
      p(1, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        DvDuS, 0.0);
    }










    /// DERIVATIVES [END]






    //    std::cout << " ------ PBicubicCoonsPatch ------" << std::endl;
    //    if (std::abs(1 - u) < 1e-7) {
    //      std::cout << "u: " << u << ", v: " << v << std::endl;
    //      std::cout << "Hu: " << std::endl << Hu << std::endl;
    //      std::cout << "Hv: " << std::endl << Hv << std::endl;
    //      std::cout << "cV: " << std::endl << cV << std::endl;
    //      std::cout << "gU: " << std::endl << gU << std::endl;
    //      //    std::cout << "M: " << std::endl << M << std::endl;
    //      std::cout << "S1: " << std::endl << S1 << std::endl;
    //      std::cout << "S2: " << std::endl << S2 << std::endl;
    //      std::cout << "S3: " << std::endl << S3 << std::endl;
    //      std::cout << "S: " << std::endl << S << std::endl;
    //    }

    return p;
  }


}   // namespace gmlib2::parametric

#endif   // GM2_PBICUBICCOONSPATCH_H
