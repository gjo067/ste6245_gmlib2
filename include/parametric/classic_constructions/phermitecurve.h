#ifndef GM_PHERMITECURVE_H
#define GM_PHERMITECURVE_H



#include "../pcurve.h"
#include "../../core/hermitebasisgenerators.h"
#include "../../core/utils.h"

#include <iostream>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 1>
  class PHermiteCurveP2V2
    : public PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
  public:
    using Base = PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor
    PHermiteCurveP2V2(const Point&  p0 = Point{0.0, 0.0, 0.0},
                      const Point&  p1 = Point{1.0, 0.0, 0.0},
                      const Vector& v0 = Vector{1.0, 0.0, 0.0},
                      const Vector& v1 = Vector{1.0, 0.0, 0.0})
      : m_c{p0, p1, v0, v1}
    {
    }

    template <typename... Ts>
    PHermiteCurveP2V2(const Point& p0, const Point& p1, const Vector& v0,
                      const Vector& v1, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c{p0, p1, v0, v1}
    {
    }

    // Members
    ColVectorT<Vector, 4> m_c;

    // PCurve interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PBoolArray
  PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PSpacePoint
  PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PSpacePoint
  PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{1};
  }


  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP2V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::EvaluationResult
  PHermiteCurveP2V2<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    auto M   = basis::generateHermiteBasisMatrixF2D2<Unit_Type, 2>(t);
    auto p_H = M * m_c;

    p[0] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(p_H[0],
                                                                         1.0);
    if (no_derivatives)
      p[1] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[1], 0.0);
    if (no_derivatives > 1)
      p[2] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[2], 0.0);


    //    std::cout << " ------ PHermiteCurveP2V2 ------" << std::endl;
    //    std::cout << "t: " << t << std::endl;
    //    std::cout << "p[0]: " << std::endl << p[0] << std::endl;


    return p;
  }










  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 1>
  class PHermiteCurveP3V2
    : public PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor
    PHermiteCurveP3V2(const Point&  p0 = Point{0.0, 0.0, 0.0},
                      const Point&  p1 = Point{0.5, 0.0, 0.0},
                      const Point&  p2 = Point{1.0, 0.0, 0.0},
                      const Vector& v0 = Vector{1.0, 0.0, 0.0},
                      const Vector& v2 = Vector{1.0, 0.0, 0.0})
      : m_c{p0, p1, p2, v0, v2}
    {
    }

    template <typename... Ts>
    PHermiteCurveP3V2(const Point& p0, const Point& p1, const Point& p2,
                      const Vector& v0, const Vector& v2, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c{p0, p1, p2, v0, v2}
    {
    }

    // Members
    ColVectorT<Vector, 5> m_c;

    // PCurve interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PBoolArray
  PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PSpacePoint
  PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PSpacePoint
  PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{1};
  }


  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V2<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::EvaluationResult
  PHermiteCurveP3V2<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    auto M   = basis::generateHermiteBasisMatrixF3D2<Unit_Type, 2>(t);
    auto p_H = M * m_c;

    p[0] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(p_H[0],
                                                                         1.0);
    if (no_derivatives)
      p[1] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[1], 0.0);
    if (no_derivatives > 1)
      p[2] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[2], 0.0);


//    std::cout << " ------ PHermiteCurveP3V2 ------" << std::endl;
//    std::cout << "t: " << t << std::endl;
//    std::cout << "p[0]: " << std::endl << p[0] << std::endl;

    return p;
  }










  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 1>
  class PHermiteCurveP3V3
    : public PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PCurve<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor
    PHermiteCurveP3V3(const Point&  p0 = Point{0.0, 0.0, 0.0},
                      const Point&  p1 = Point{0.5, 0.0, 0.0},
                      const Point&  p2 = Point{1.0, 0.0, 0.0},
                      const Vector& v0 = Vector{1.0, 0.0, 0.0},
                      const Vector& v1 = Vector{1.0, 0.0, 0.0},
                      const Vector& v2 = Vector{1.0, 0.0, 0.0})
      : m_c{p0, p1, p2, v0, v1, v2}
    {
    }

    template <typename... Ts>
    PHermiteCurveP3V3(const Point& p0, const Point& p1, const Point& p2,
                      const Vector& v0, const Vector& v1, const Vector& v2,
                      Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c{p0, p1, p2, v0, v1, v2}
    {
    }

    // Members
    ColVectorT<Vector, 6> m_c;

    // PCurve interface
  public:
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
  };




  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PBoolArray
  PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PSpacePoint
  PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::PSpacePoint
  PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                    ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{1};
  }


  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PHermiteCurveP3V3<SpaceObjectEmbedBase_T,
                             ParametricSpaceFrameDim_T>::EvaluationResult
  PHermiteCurveP3V3<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    auto M   = basis::generateHermiteBasisMatrixF3D3<Unit_Type, 2>(t);
    auto p_H = M * m_c;

    p[0] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(p_H[0],
                                                                         1.0);
    if (no_derivatives)
      p[1] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[1], 0.0);
    if (no_derivatives > 1)
      p[2] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[2], 0.0);


//    std::cout << " ------ PHermiteCurveP3V3 ------" << std::endl;
//    std::cout << "t: " << t << std::endl;
//    std::cout << "p[0]: " << std::endl << p[0] << std::endl;


    return p;
  }




}   // namespace gmlib2::parametric



#endif   // GM_PHERMITECURVE_H
