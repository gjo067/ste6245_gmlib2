#ifndef GM2_PBILINEARCOONSPATCH_H
#define GM2_PBILINEARCOONSPATCH_H


#include "../psurface.h"
#include "../../core/utils.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T  = ProjectiveSpaceObject<>,
            size_t ParametricSpaceFrameDim_T = 2>
  class PBilinearCoonsPatch
    : public PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T> {
    using Base = PSurface<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>;

  public:
    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    using Point            = typename Base::Point_Type;
    using Vector           = typename Base::Vector_Type;
    using VectorH          = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // Constructor(s)
    template <typename... Ts>
    PBilinearCoonsPatch(PCurve<SpaceObjectEmbedBase_T>* c0,
                        PCurve<SpaceObjectEmbedBase_T>* c1,
                        PCurve<SpaceObjectEmbedBase_T>* g0,
                        PCurve<SpaceObjectEmbedBase_T>* g1, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c0{c0}, m_c1{c1}, m_g0{g0}, m_g1{g1}
    {
    }

    // Members
    PCurve<SpaceObjectEmbedBase_T>* m_c0;
    PCurve<SpaceObjectEmbedBase_T>* m_c1;
    PCurve<SpaceObjectEmbedBase_T>* m_g0;
    PCurve<SpaceObjectEmbedBase_T>* m_g1;

    bool validateConstruction() const;

    // TPSurf interface
  public:
    PBoolArray  isClosed() const final;
    PSpacePoint startParameters() const final;
    PSpacePoint endParameters() const final;

  protected:
    EvaluationResult evaluate(
      const PSpacePoint& par, const PSizeArray& no_der,
      const PBoolArray& from_left
      = utils::initStaticContainer<PBoolArray, PVectorDim>(true)) const final;
  };





  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  bool
  PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                      ParametricSpaceFrameDim_T>::validateConstruction() const
  {
    auto eval_start = [](auto* c) {
      return c->evaluateParent(c->startParameters(), {{0}})[0];
    };

    auto eval_end
      = [](auto* c) { return c->evaluateParent(c->endParameters(), {{0}})[0]; };

    auto checkError
      = [](auto p0, auto p1) { return blaze::length(p0 - p1) < 1e-7; };

    auto c0s = eval_start(m_c0);
    auto c0e = eval_end(m_c0);
    auto c1s = eval_start(m_c1);
    auto c1e = eval_end(m_c1);
    auto g0s = eval_start(m_g0);
    auto g0e = eval_end(m_g0);
    auto g1s = eval_start(m_g1);
    auto g1e = eval_end(m_g1);

    // clang-format off
    return     checkError(c0s, g0s) and checkError(c0e, g1s)
           and checkError(c1s, g0e) and checkError(c1e, g1e);
    // clang-format on
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                               ParametricSpaceFrameDim_T>::PBoolArray
  PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                      ParametricSpaceFrameDim_T>::isClosed() const
  {
    return {{false, false}};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                               ParametricSpaceFrameDim_T>::PSpacePoint
  PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                      ParametricSpaceFrameDim_T>::startParameters() const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                               ParametricSpaceFrameDim_T>::PSpacePoint
  PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                      ParametricSpaceFrameDim_T>::endParameters() const
  {
    return PSpacePoint{1, 1};
  }

  template <typename SpaceObjectEmbedBase_T, size_t ParametricSpaceFrameDim_T>
  typename PBilinearCoonsPatch<SpaceObjectEmbedBase_T,
                               ParametricSpaceFrameDim_T>::EvaluationResult
  PBilinearCoonsPatch<SpaceObjectEmbedBase_T, ParametricSpaceFrameDim_T>::
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left) const
  {
    const auto u    = par[0];
    const auto v    = par[1];
    const auto nd_u = no_der[0];
    const auto nd_v = no_der[1];
    const auto fl_u = from_left[0];
    const auto fl_v = from_left[1];

    EvaluationResult p(nd_u + 1, nd_v + 1);

    auto par_delta = endParameters() - startParameters();
    auto u_delta   = par_delta[0];
    auto v_delta   = par_delta[1];

    auto c0_s = m_c0->startParameters();
    auto c0_e = m_c0->endParameters();
    auto c0_d = c0_e - c0_s;
    auto c1_s = m_c1->startParameters();
    auto c1_e = m_c1->endParameters();
    auto c1_d = c1_e - c1_s;
    auto g0_s = m_g0->startParameters();
    auto g0_e = m_g0->endParameters();
    auto g0_d = g0_e - g0_s;
    auto g1_s = m_g1->startParameters();
    auto g1_e = m_g1->endParameters();
    auto g1_d = g1_e - g1_s;

    //    std::cout << "par delta: " << par_delta << std::endl;
    //    std::cout << "u delta: " << u_delta << std::endl;
    //    std::cout << "v delta: " << v_delta << std::endl;

    //    std::cout << "c0_s: " << c0_s << std::endl;
    //    std::cout << "c0_e: " << c0_e << std::endl;
    //    std::cout << "c1_s: " << c1_s << std::endl;
    //    std::cout << "c1_e: " << c1_e << std::endl;
    //    std::cout << "d0_s: " << d0_s << std::endl;
    //    std::cout << "d0_e: " << d0_e << std::endl;
    //    std::cout << "d1_s: " << d1_s << std::endl;
    //    std::cout << "d1_e: " << d1_e << std::endl;

    auto c0_v_map = v * (c0_d / v_delta);   // component-wise division
    auto c1_v_map = v * (c1_d / v_delta);   // component-wise division
    auto g0_u_map = u * (g0_d / u_delta);   // component-wise division
    auto g1_u_map = u * (g1_d / u_delta);   // component-wise division

    //    std::cout << "c0_par_delta: " << c0_par_delta << std::endl;
    //    std::cout << "c0_u_map: " << c0_u_map << std::endl;
    //    std::cout << "c1_par_delta: " << c1_par_delta << std::endl;
    //    std::cout << "c1_u_map: " << c1_u_map << std::endl;

    //    std::cout << "d0_par_delta: " << d0_par_delta << std::endl;
    //    std::cout << "d0_v_map: " << d0_v_map << std::endl;
    //    std::cout << "d1_par_delta: " << d1_par_delta << std::endl;
    //    std::cout << "d1_v_map: " << d1_v_map << std::endl;

    //    std::cout << "u_c0_mapped: " << u_c0_mapped << std::endl;
    //    std::cout << "u_c1_mapped: " << u_c1_mapped << std::endl;
    //    std::cout << "v_d0_mapped: " << v_d0_mapped << std::endl;
    //    std::cout << "v_d1_mapped: " << v_d1_mapped << std::endl;

    auto c0 = m_c0->evaluateParent(c0_v_map, {{nd_v}}, {{fl_v}});
    auto c1 = m_c1->evaluateParent(c1_v_map, {{nd_v}}, {{fl_v}});
    auto g0 = m_g0->evaluateParent(g0_u_map, {{nd_u}}, {{fl_u}});
    auto g1 = m_g1->evaluateParent(g1_u_map, {{nd_u}}, {{fl_u}});

    // A = c0_s = g0_s, B = c0_e = g1_s, C = c1_s = g0_e, D = c1_e = g1_e
    auto A = m_c0->evaluateParent(c0_s, {{nd_v}}, {{fl_v}});
    auto B = m_c0->evaluateParent(c0_e, {{nd_v}}, {{fl_v}});
    auto C = m_c1->evaluateParent(c1_s, {{nd_v}}, {{fl_v}});
    auto D = m_c1->evaluateParent(c1_e, {{nd_v}}, {{fl_v}});

    //    std::cout << "c0: " << c0 << std::endl;
    //    std::cout << "c1: " << c1 << std::endl;
    //    std::cout << "d0: " << d0 << std::endl;
    //    std::cout << "d1: " << d1 << std::endl;

    auto Bu   = ColVectorT<Unit_Type, 2>{1 - u, u};
    auto DuBu = ColVectorT<Unit_Type, 2>{-1, 1};
    auto Bv   = RowVectorT<Unit_Type, 2>{1 - v, v};
    auto DvBv = RowVectorT<Unit_Type, 2>{-1, 1};
    auto cV   = RowVectorT<VectorH, 2>{c0[0], c1[0]};
    auto DvcV = RowVectorT<VectorH, 2>{c0[1], c1[1]};
    auto gU   = ColVectorT<VectorH, 2>{g0[0], g1[0]};
    auto DugU = ColVectorT<VectorH, 2>{g0[1], g1[1]};
    auto cM   = SqMatrixT<VectorH, 2>{{A[0], C[0]}, {B[0], D[0]}};

    auto S1 = cV * Bu;
    auto S2 = Bv * gU;
    auto S3 = Bv * cM * Bu;


    //    std::cout << "S1: " << S1 << std::endl;
    //    std::cout << "S2: " << S2 << std::endl;
    //    std::cout << "S3: " << S3 << std::endl;

    // S
    p(0, 0) = S1 + S2 - S3;

    // DuS
    if (nd_u) {
      auto DuS1 = /* DucV * Bu + */ cV * DuBu;
      auto DuS2 = /* DuBv * gV + */ Bv * DugU;
      auto DuS3 = /* DuBv * cM * Bu + Bv * DucM * Bu + */ Bv * cM * DuBu;
      p(1, 0)   = DuS1 + DuS2 - DuS3;
    }

    // DvS : DvcM = DvgM = 0
    if (nd_v) {
      auto DvS1 = DvcV * Bu;      /* + cV * DvBu */
      auto DvS2 = DvBv * gU;      /* + Bv * DvgU */
      auto DvS3 = DvBv * cM * Bu; /* + Bv * DvcM * Bu + Bv * cM * DvBu */
      p(0, 1)   = DvS1 + DvS2 - DvS3;
    }

    // DvDuS = DuDvS
    if (nd_u and nd_v) {
      auto DvDuS1 = DvcV * DuBu;
      auto DvDuS2 = DvBv * DugU;
      auto DvDuS3 = DvBv * cM * DuBu;
      p(1, 1)     = DvDuS1 + DvDuS2 - DvDuS3;
    }


    //    std::cout << "p: " << p(0, 0) << std::endl;
    //    std::cout << "pu: " << p(1, 0) << std::endl;
    //    std::cout << "pv: " << p(0, 1) << std::endl;
    //    std::cout << "puv: " << p(1, 1) << std::endl;

    return p;
  }





}   // namespace gmlib2::parametric

#endif   // GM2_PBILINEARCOONSPATCH_H
