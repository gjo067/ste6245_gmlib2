#ifndef GM_PSUBSPACEOBJECT_H
#define GM_PSUBSPACEOBJECT_H



#include "pcurve.h"
#include "parametricobject.h"
#include "../core/utils.h"

// stl
#include <type_traits>
#include <iostream>

namespace gmlib2::parametric
{


  namespace detail::parametricsubobject
  {

    //////////////////
    /// evaluate(...)
    /// SubCurve in Curve
    ///
    template <typename EvaluationResult_T, typename PSpaceCurve_T,
              typename ParametricCurve_T, typename PVector_T,
              typename PSizeArray_T, typename PBoolArray_T>
    EvaluationResult_T evaluate(const PSpaceCurve_T& pspace_curve,
                                ParametricCurve_T* pcurve, const PVector_T& par,
                                const PSizeArray_T& no_der,
                                const PBoolArray_T& /*from_left*/,
                                objecttype::pcurve_tag, objecttype::pcurve_tag)
    {
      //      std::cout << " ----- PSubCurve - Curve ----- " << std::endl;
      //      std::cout << " sub-curve par: " << std::endl << par << std::endl;

      // Evaluate pspace curve -- result in homogeneous coords ^^,
      auto pcurve_par = pspace_curve.evaluateParent(par, no_der, {{true}});
      //      std::cout << " pcurve_par: " << std::endl << pcurve_par <<
      //      std::endl;

      // Make pspace curve result not homeogeneous coords
      auto pcurve_par_input
        = blaze::subvector(pcurve_par[0UL], 0UL, pcurve_par.size() - 1);
      //      std::cout << " pcurve_par_input: " << std::endl <<
      //      pcurve_par_input << std::endl;

      // Evaluate surface along pspace curve
      auto eval = pcurve->evaluateParent(pcurve_par_input, no_der, {{true}});
      //      std::cout << " eval: " << std::endl << eval << std::endl;

      // Copy the result and scale the derivatives.
      // The scaling is not (yet) sane for no_der > 1.
      EvaluationResult_T p{eval};

      auto d_scale{0.0};
      for (size_t i{1}; i <= no_der[0]; ++i) {
        d_scale
          += blaze::length(
               blaze::subvector(pcurve_par[i], 0UL, pcurve_par.size() - 1))
             / (pcurve->endParameters()[0] - pcurve->startParameters()[0]);
        p[i] *= d_scale;
      }

      //      std::cout << " p: " << std::endl << p << std::endl;

      return p;
    }



    ////////////////////
    /// evaluate(...)
    /// SubCurve on Surface
    ///
    template <typename EvaluationResult_T, typename PSpaceCurve_T,
              typename ParametricSurface_T, typename PVector_T,
              typename PSizeArray_T, typename PBoolArray_T>
    EvaluationResult_T
    evaluate(const PSpaceCurve_T& pspace_curve, ParametricSurface_T* psurface,
             const PVector_T& par, const PSizeArray_T& no_der,
             const PBoolArray_T& /*from_left*/, objecttype::pcurve_tag,
             objecttype::psurface_tag)
    {
      //      std::cout << " ----- PSubCurve - Surface ----- " << std::endl;
      //      std::cout << " sub-curve par: " << std::endl << par << std::endl;

      // Evaluate pspace curve -- result in homogeneous coords ^^,
      auto psurface_par
        = pspace_curve.evaluateParent(par, {{0}}, {{true}})[0UL];
      //      std::cout << " psurface_par: " << std::endl << psurface_par <<
      //      std::endl;

      // Make pspace curve result not homeogeneous coords
      auto psurface_par_input
        = blaze::subvector(psurface_par, 0UL, psurface_par.size() - 1);
      //      std::cout << " psurface_par_input: " << std::endl <<
      //      psurface_par_input << std::endl;

      // Evaluate surface along pspace curve
      auto eval = psurface->evaluateParent(psurface_par_input, {{0, 0}},
                                           {{true, true}});
      //      std::cout << " eval: " << std::endl << eval << std::endl;

      // Copy and stuff
      EvaluationResult_T p(no_der[0] + 1);
      p[0] = eval(0, 0);
      p[1] = {1, 0, 0, 0};

      //      std::cout << " p: " << std::endl << p << std::endl;

      return p;
    }





    ////////////////////
    /// evaluate(...)
    /// SubCurve on Polygon
    ///
    template <typename EvaluationResult_T, typename PSpaceCurve_T,
              typename ParametricPolygon_T, typename PVector_T,
              typename PSizeArray_T, typename PBoolArray_T>
    EvaluationResult_T
    evaluate(const PSpaceCurve_T& pspace_curve, ParametricPolygon_T* ppolygon,
             const PVector_T& par, const PSizeArray_T& /*no_der*/,
             const PBoolArray_T& /*from_left*/, objecttype::pcurve_tag,
             objecttype::ppolygonsurface_tag)
    {
      // Evaluate pspace curve -- result in homogeneous coords ^^,
      auto ppolygon_par
        = pspace_curve.evaluateParent(par, {{0}}, {{true}})[0UL];

      // Evaluate polygon along pspace curve
      constexpr auto PPolygon_PVectorDim = ParametricPolygon_T::PVectorDim;
      using PPolygon_PSizeArray = typename ParametricPolygon_T::PSizeArray;
      using PPolygon_PBoolArray = typename ParametricPolygon_T::PBoolArray;
      auto eval                 = ppolygon->evaluateParent(
        blaze::subvector(ppolygon_par, 0UL, ppolygon_par.size() - 1),
        utils::initStaticContainer<PPolygon_PSizeArray, PPolygon_PVectorDim>(1UL),
        utils::initStaticContainer<PPolygon_PBoolArray, PPolygon_PVectorDim>(
          true));

      // Copy and stuff
      EvaluationResult_T p(1 + 1);
      p[0] = eval[0];
      p[1] = {1, 0, 0, 0};

      //      std::cout << " ----- PSubCurve - Polygon ----- " << std::endl;
      //      std::cout << " sub-curve par: " << std::endl << par << std::endl;
      //      std::cout << " ppolygon_par: " << std::endl << ppolygon_par <<
      //      std::endl; std::cout << " eval: " << std::endl << eval <<
      //      std::endl; std::cout << " p: " << std::endl << p << std::endl;

      return p;
    }





    ////////////////////
    /// evaluate(...)
    /// SubSurface on Surface
    ///
    template <typename EvaluationResult_T, typename PSpaceSurface_T,
              typename ParametricSurface_T, typename PVector_T,
              typename PSizeArray_T, typename PBoolArray_T>
    EvaluationResult_T
    evaluate(const PSpaceSurface_T& pspace_surface,
             ParametricSurface_T* psurface, const PVector_T& par,
             const PSizeArray_T& no_der, const PBoolArray_T& /*from_left*/,
             objecttype::psurface_tag, objecttype::psurface_tag)
    {
      //      std::cout << " ----- PSubSurface - Surface ----- " << std::endl;
      //      std::cout << " sub-surface par: " << std::endl << par <<
      //      std::endl;

      // Evaluate pspace curve -- result in homogeneous coords ^^,
      auto psurface_par = pspace_surface.evaluateParent(
        par, {{0, 0}}, {{true, true}})(0UL, 0UL);
      //      std::cout << " psurface_par: " << std::endl << psurface_par <<
      //      std::endl;

      // Make pspace curve result not homeogeneous coords
      auto psurface_par_input
        = blaze::subvector(psurface_par, 0UL, psurface_par.size() - 1);
      //      std::cout << " psurface_par_input: " << std::endl <<
      //      psurface_par_input << std::endl;

      // Evaluate surface along pspace curve
      auto eval = psurface->evaluateParent(psurface_par_input, {{0, 0}},
                                           {{true, true}});
      //      std::cout << " eval: " << std::endl << eval << std::endl;

      // Copy and stuff
      EvaluationResult_T p(no_der[0] + 1, no_der[1] + 1);
      p(0, 0) = eval(0, 0);
      //      p[1] = {1, 0, 0, 0};

      //      std::cout << " p: " << std::endl << p << std::endl;

      return p;
    }








    ////////////////////
    /// evaluate(...)
    /// SubSurface on PolygonSurface
    ///
    template <typename EvaluationResult_T, typename PSpaceSurface_T,
              typename ParametricPolygonSurface_T, typename PVector_T,
              typename PSizeArray_T, typename PBoolArray_T>
    EvaluationResult_T
    evaluate(const PSpaceSurface_T&      pspace_surface,
             ParametricPolygonSurface_T* ppolygon, const PVector_T& par,
             const PSizeArray_T& no_der, const PBoolArray_T& /*from_left*/,
             objecttype::psurface_tag, objecttype::ppolygonsurface_tag)
    {
      //      std::cout << " ----- PSubSurface - Surface ----- " << std::endl;
      //      std::cout << " sub-surface par: " << std::endl << par <<
      //      std::endl;



      // Types  ---- WE NEED TYPETRAITS!!
      constexpr auto PPolygon_VectorDim
        = ParametricPolygonSurface_T::VectorDim;
      using PPolygon_VectorH = typename ParametricPolygonSurface_T::VectorH_Type;



      constexpr auto PPolygon_PVectorDim
        = ParametricPolygonSurface_T::PSpace_VectorDim;
      using PPolygon_Unit_Type = typename ParametricPolygonSurface_T::Unit_Type;
      using PPolygon_PSpacePoint =
        typename ParametricPolygonSurface_T::PSpace_Point_Type;
      using PPolygon_PSizeArray =
        typename ParametricPolygonSurface_T::PSpace_SizeArray_Type;
      using PPolygon_PBoolArray =
        typename ParametricPolygonSurface_T::PSpace_BoolArray_Type;
      // Types  ---- WE NEED TYPETRAITS !! END AND OUT !!



      // Evaluate pspace curve -- result in homogeneous coords ^^,
      auto pspace_surface_eval = pspace_surface.evaluateParent(
        par, no_der, {{true, true}});

      auto ppolygon_par_pt = blaze::subvector(pspace_surface_eval(0UL, 0UL),
                                              0UL, PPolygon_PVectorDim);
      auto ppolygon_par_u = blaze::subvector(pspace_surface_eval(1UL, 0UL), 0UL,
                                             PPolygon_PVectorDim);
      auto ppolygon_par_v = blaze::subvector(pspace_surface_eval(0UL, 1UL), 0UL,
                                             PPolygon_PVectorDim);
      auto ppolygon_par_uv = blaze::subvector(pspace_surface_eval(1UL, 1UL), 0UL,
                                             PPolygon_PVectorDim);

      // Evaluate polygon over pspace surface
      auto eval = ppolygon->evaluateParent(
        blaze::subvector(ppolygon_par_pt, 0UL, PPolygon_PVectorDim),
        utils::initStaticContainer<PPolygon_PSizeArray, PPolygon_PVectorDim>(0UL),
        utils::initStaticContainer<PPolygon_PBoolArray, PPolygon_PVectorDim>(
          true));


      EvaluationResult_T p(no_der[0]+1, no_der[1]+1);
      p(0, 0) = eval[0];

      VectorT<PPolygon_VectorH,PPolygon_PVectorDim> vps;
      for( size_t vi = 0; vi < PPolygon_PVectorDim; ++vi) {

        auto v_par
          = utils::initStaticContainer<PPolygon_PSpacePoint, PPolygon_PVectorDim>(
            PPolygon_Unit_Type(0));
        v_par[vi] = 1;

        vps[vi] = ppolygon->evaluateParent(
          v_par,
          utils::initStaticContainer<PPolygon_PSizeArray, PPolygon_PVectorDim>(
            0UL),
          utils::initStaticContainer<PPolygon_PBoolArray, PPolygon_PVectorDim>(
            true))[0UL];
      }

      if(no_der[0]) {
        p(1,0) = blaze::inner(vps,ppolygon_par_u);
      }

      if(no_der[1]) {
        p(0,1) = blaze::inner(vps,ppolygon_par_v);
      }

      if(no_der[0] and no_der[1]) {
        p(1,1) = blaze::inner(vps,ppolygon_par_uv);
      }


      //      std::cout << " ----- PSubSurface - Surface ----- " << std::endl;
      //      std::cout << " sub-surface par: " << std::endl << par <<
      //      std::endl; std::cout << " ppolygon_par: " << std::endl <<
      //      ppolygon_par << std::endl; std::cout << " eval: " << std::endl <<
      //      eval << std::endl; std::cout << " p: " << std::endl << p <<
      //      std::endl;
      return p;
    }










    ////////////////////
    /// evaluate(...)
    /// SubPolygonSurface on PolygonSurface
    ///
    template <typename EvaluationResult_T, typename PSpacePolygonSurface_T,
              typename ParametricPolygonSurface_T, typename PVector_T,
              typename PSizeArray_T, typename PBoolArray_T>
    EvaluationResult_T
    evaluate(const PSpacePolygonSurface_T& pspace_polygonsurface,
             ParametricPolygonSurface_T* ppolygonsurface, const PVector_T& par,
             const PSizeArray_T& /*no_der*/, const PBoolArray_T& /*from_left*/,
             objecttype::ppolygonsurface_tag, objecttype::ppolygonsurface_tag)
    {
      //      std::cout << " ----- PSubPolygonSurface - PolygonSurface ----- "
      //      << std::endl; std::cout << " sub-polygonsurface par: " <<
      //      std::endl << par << std::endl;

      // Evaluate pspace polygon -- result in homogeneous coords ^^,
      auto ppolygonsurface_par = pspace_polygonsurface.evaluateParent(
        par,
        utils::initStaticContainer<typename PSpacePolygonSurface_T::PSizeArray,
                                   PSpacePolygonSurface_T::PVectorDim>(0UL),
        utils::initStaticContainer<typename PSpacePolygonSurface_T::PBoolArray,
                                   PSpacePolygonSurface_T::PVectorDim>(
          true))[0UL];
      //      std::cout << " psurface_par: " << std::endl << ppolygonsurface_par
      //      << std::endl;

      // Make pspace curve result not homeogeneous coords
      auto ppolygonsurface_par_input = blaze::subvector(
        ppolygonsurface_par, 0UL, ppolygonsurface_par.size() - 1);
      //      std::cout << " ppolygonsurface_par_input: " << std::endl <<
      //      ppolygonsurface_par_input << std::endl;

      // Evaluate surface along pspace curve
      auto eval = ppolygonsurface->evaluateParent(
        ppolygonsurface_par_input,
        utils::initStaticContainer<
          typename ParametricPolygonSurface_T::PSpace_SizeArray_Type,
          ParametricPolygonSurface_T::PSpace_VectorDim>(0UL),
        utils::initStaticContainer<
          typename ParametricPolygonSurface_T::PSpace_BoolArray_Type,
          ParametricPolygonSurface_T::PSpace_VectorDim>(true));
      //      std::cout << " eval: " << std::endl << eval << std::endl;


      // TODO: -- may potentially break if EvaluationResult
      // differs or content of Evaluation Result differs

      // Relay polygon-evaluation
      return eval;
    }

  }   // namespace detail::parametricsubobject









  //  template <template <typename, size_t> class PSpaceObject_T,
  //            typename PSpaceObject_Tag_T, typename ParametricObject_T>
  //  class ParametricSubObject
  //    : public ParametricObject<
  //        typename PSpaceObject_T<
  //          ProjectiveSpaceObject<typename ParametricObject_T::PSpaceInfo>,
  //          1UL>::PSpaceInfo,   /// PSpace
  //        PSpaceObject_Tag_T,
  //        typename ParametricObject_T::ParametricObjectEmbedBase> {   ///
  //        ESpace
  //  public:
  //    using Base = ParametricObject<
  //      typename PSpaceObject_T< ProjectiveSpaceObject<typename
  //      ParametricObject_T::PSpaceInfo>, 1UL>::PSpaceInfo, PSpaceObject_Tag_T,
  //      typename ParametricObject_T::ParametricObjectEmbedBase>;


  //  template <typename PSpaceObject_T, typename ParametricObject_T,
  //            typename Enable = void>
  //  class ParametricSubObject;

  template <typename PSpaceObject_T, typename ParametricObject_T>
  class ParametricSubObject
    //      <
    //    PSpaceObject_T, ParametricObject_T,
    //    std::enable_if_t<std::is_same<
    //      typename PSpaceObject_T::PSpaceInfo,
    //      typename ParametricObject_T::EmbedSpace::EmbedSpaceInfo>::value> >
    : public ParametricObject<
        typename PSpaceObject_T::PSpaceInfo,
        typename PSpaceObject_T::ParametricObjectType_Tag,
        typename ParametricObject_T::ParametricObjectEmbedBase,
        detail::Sampler> {   /// ESpace
  public:
    using Base
      = ParametricObject<typename PSpaceObject_T::PSpaceInfo,
                         typename PSpaceObject_T::ParametricObjectType_Tag,
                         typename ParametricObject_T::ParametricObjectEmbedBase,
                         detail::Sampler>;


    using PSpaceObject_Type = PSpaceObject_T;

    //    using PSpaceObject_Type_ESpaceType
    //      = ProjectiveSpaceObject<typename ParametricObject_T::PSpaceInfo>;
    //    using PSpaceObject_Type
    //      = PSpaceObject_T<PSpaceObject_Type_ESpaceType, 1UL>;

    //    using A = ParametricObject_T;
    //    using PSpaceObject_Type
    //      = PSpaceObject_T<spaces::ParameterFVSpaceInfo<A::FramDim,
    //      A::VectorDim>,
    //                       PSpaceObject_Tag_T,
    //                       ProjectiveSpaceObject<typename
    //                       ParametricObject_T::PSpaceInfo>,detail::Sampler>;
    //    using PSpaceObject_Type     = PSpaceObject_T;
    using ParametricObject_Type = ParametricObject_T;

    // Types
    using Unit_Type        = typename Base::Unit_Type;
    using EvaluationResult = typename Base::EvaluationResult;
    using Point            = typename Base::Point_Type;
    using Vector           = typename Base::Vector_Type;
    using VectorH          = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = PSpaceObject_Type::PSpace_VectorDim;
    using PSpacePoint  = typename PSpaceObject_Type::PSpace_Point_Type;
    using PSpaceVector = typename PSpaceObject_Type::PSpace_Vector_Type;
    using PSizeArray   = typename PSpaceObject_Type::PSpace_SizeArray_Type;
    using PBoolArray   = typename PSpaceObject_Type::PSpace_BoolArray_Type;

    // Constructor
    template <typename... Ts>
    ParametricSubObject(ParametricObject_Type* parametric_object, Ts&&... ts)
      : Base(), m_pspace_object{std::forward<Ts>(ts)...}, m_parametric_object{
                                                            parametric_object}
    {
      // clang-format off
//      std::cout << " --- Parametric SubObject --- " << std::endl;
//      std::cout << "Subcurve:" << std::endl;
//      std::cout << " * param fdim: " << Base::PSpace_FrameDim << std::endl;
//      std::cout << " * param vdim: " << Base::PSpace_VectorDim << std::endl;
//      std::cout << " * embed fdim: " << Base::FrameDim << std::endl;
//      std::cout << " * embed vdim: " << Base::VectorDim << std::endl;
//      std::cout << "--" << std::endl;
//      std::cout << "Parametric object - pspace curve:" << std::endl;
//      std::cout << " * param fdim: " << PSpaceObject_Type::PSpace_FrameDim << std::endl;
//      std::cout << " * param vdim: " << PSpaceObject_Type::PSpace_VectorDim << std::endl;
//      std::cout << " * embed fdim: " << PSpaceObject_Type::FrameDim << std::endl;
//      std::cout << " * embed vdim: " << PSpaceObject_Type::VectorDim << std::endl;
//      std::cout << "--" << std::endl;
//      std::cout << "Parametric object:" << std::endl;
//      std::cout << " * param fdim: " << ParametricObject_Type::PSpace_FrameDim << std::endl;
//      std::cout << " * param vdim: " << ParametricObject_Type::PSpace_VectorDim << std::endl;
//      std::cout << " * embed fdim: " << ParametricObject_Type::FrameDim << std::endl;
//      std::cout << " * embed vdim: " << ParametricObject_Type::VectorDim << std::endl;
//      std::cout << "--" << std::endl;
      // clang-format on
    }

    // Members
    PSpaceObject_Type      m_pspace_object;
    ParametricObject_Type* m_parametric_object;

    // PCurve interface
  public:
    PBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PBoolArray, PVectorDim>(false);
    }
    PSpacePoint startParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PVectorDim>(Unit_Type(0));
    }
    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PVectorDim>(Unit_Type(1));
    }

  protected:
    EvaluationResult evaluate(
      const PSpacePoint& par, const PSizeArray& no_derivatives,
      const PBoolArray& from_left
      = utils::initStaticContainer<PBoolArray, PVectorDim>(true)) const override
    {
      return detail::parametricsubobject::evaluate<EvaluationResult>(
        m_pspace_object, m_parametric_object, par, no_derivatives, from_left,
        typename PSpaceObject_Type::ParametricObjectType_Tag(),
        typename ParametricObject_Type::ParametricObjectType_Tag());
    }
  };



  //////////////////////
  /// ParametricSubCurve
  template <template <typename, size_t> class PSpaceCurve_T,
            typename ParametricObject_T, size_t PSpaceCurveFrameDim_T = 1>
  using PSubCurve = ParametricSubObject<
    PSpaceCurve_T<ProjectiveSpaceObject<typename ParametricObject_T::PSpaceInfo>,
                  PSpaceCurveFrameDim_T>,
    ParametricObject_T>;




  ////////////////////////
  /// ParametricSubSurface
  template <template <typename, size_t> class PSpaceSurface_T,
            typename ParametricObject_T, size_t PSpaceSurfaceFrameDim_T = 2>
  using PSubSurface = ParametricSubObject<
    PSpaceSurface_T<ProjectiveSpaceObject<typename ParametricObject_T::PSpaceInfo>,
                    PSpaceSurfaceFrameDim_T>,
    ParametricObject_T>;




  ///////////////////////////////
  /// ParametricSubPolygonSurface
  template <template <size_t, typename, size_t> class PSpacePolygonSurface_T,
            size_t PSpacePolygonSurfaceVectorDim_T, typename ParametricObject_T,
            size_t PSpacePolygonSurfaceFrameDim_T
            = PSpacePolygonSurfaceVectorDim_T>
  using PSubPolygonSurface = ParametricSubObject<
    PSpacePolygonSurface_T<PSpacePolygonSurfaceVectorDim_T,
                           ProjectiveSpaceObject<typename ParametricObject_T::PSpaceInfo>,
                           PSpacePolygonSurfaceFrameDim_T>,
    ParametricObject_T>;


}   // namespace gmlib2::parametric



#endif   // GM_PSUBSPACEOBJECT_H
