#ifndef GM2_PARAMETRIC_STUFF_POLYGONS_PGENERALIZEDBEZIERPATCH_H
#define GM2_PARAMETRIC_STUFF_POLYGONS_PGENERALIZEDBEZIERPATCH_H

// gmlib2
#include "../../parametricobject.h"
#include "../../stuff/gbcutils.h"
#include "../../../core/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  namespace detail
  {

    template <typename PGBP_TYPE_T>
    inline auto constructPControlNetD(
      const VectorT<typename PGBP_TYPE_T::Point, PGBP_TYPE_T::PVectorDim>& P,
      const int& d, const int& n)
    {


      constexpr auto N      = int(PGBP_TYPE_T::PVectorDim);
      using Unit_Type       = typename PGBP_TYPE_T::Unit_Type;
      using PSpaceUnit_Type = typename PGBP_TYPE_T::PSpaceUnit_Type;
      using ControlNet      = typename PGBP_TYPE_T::ControlNet;

      //      const auto N   = int(P.size());
      const int nm2 = (n + N - 2) % N;
      const int nm1 = (n + N - 1) % N;
      const int np1 = (n + N + 1) % N;

      ColVectorT<PSpaceUnit_Type, N> anm2(0);
      anm2[size_t(nm2)] = 1.0;
      ColVectorT<PSpaceUnit_Type, N> anm1(0);
      anm1[size_t(nm1)] = 1.0;
      ColVectorT<PSpaceUnit_Type, N> an(0);
      an[size_t(n)] = 1.0;
      ColVectorT<PSpaceUnit_Type, N> anp1(0);
      anp1[size_t(np1)] = 1.0;



      const auto P_regular
        = utils::parametric::generateRegularPolygon2D<N, Unit_Type,
                                                      PSpaceUnit_Type>();


      const auto D  = (d + 2) / 2;
      const auto L  = (d + 1) / 2;
      auto       CN = ControlNet(size_t(L), size_t(D));

      const PSpaceUnit_Type dt = PSpaceUnit_Type(1) / PSpaceUnit_Type(d);


      for (int r = 0; r < L; ++r) {
        for (int c = 0; c < D; ++c) {

          const auto ir = r * dt;   // i-1 side with respect to side i
          const auto ic = c * dt;   // i+1 side with respect to side i

          const auto gamma_im1_alpha = anm1 + ic * (anm2 - anm1);
          const auto gamma_ip1_alpha = an + ic * (anp1 - an);

          const auto alpha_regular = ColVectorT<PSpaceUnit_Type, N>(
            gamma_ip1_alpha + ir * (gamma_im1_alpha - gamma_ip1_alpha));

          const auto alpha = utils::gbc::mvcFromGbc(P_regular, alpha_regular);


          const auto p_rc = blaze::inner(P,alpha);

          CN(size_t(r), size_t(c)) = p_rc;
        }
      }

      using ReturnType = const ControlNet;
      return std::forward<ReturnType>(blaze::eval(CN));
    }

    template <typename PGBP_TYPE_T>
    inline auto constructPControlNetsD(
      const VectorT<typename PGBP_TYPE_T::Point, PGBP_TYPE_T::PVectorDim>& P,
      const int& d, const int& is = 0)
    {

      constexpr auto N  = PGBP_TYPE_T::PVectorDim;
      using ControlNets = typename PGBP_TYPE_T::ControlNets;

      ControlNets cns;
      for( size_t i = size_t(is); i < N; ++i )
        cns[i] = constructPControlNetD<PGBP_TYPE_T>(P,d, (N+i)%N);

      using ReturnType = const ControlNets;
      return std::forward<ReturnType>(blaze::eval(cns));
    }




    template <typename Unit_Type_T, size_t N_T>
    inline auto rotateLambda(const VectorT<Unit_Type_T, N_T>& Lambda, int i)
    {
      const int j = (int(N_T) + i) % int(N_T);

      VectorT<Unit_Type_T, N_T> r_Lambda;
      std::rotate_copy(std::begin(Lambda), std::begin(Lambda) + j,
                       std::end(Lambda), std::begin(r_Lambda));
      return r_Lambda;
    }

  }   // namespace detail

  template <size_t ParameterSpaceVectorDim_T,
            typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            size_t ParameterSpaceFrameDim_T = ParameterSpaceVectorDim_T>
  class PGeneralizedBezierPatch
    : public PPolygonSurface<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                             ParameterSpaceFrameDim_T> {
    using Base
      = PPolygonSurface<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                        ParameterSpaceFrameDim_T>;

  public:
    // Types
    static constexpr auto VectorDim = Base::VectorDim;
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    // Paramter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpaceUnit_Type            = typename Base::PSpace_Unit_Type;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;


    using ControlNet  = DMatrixT<Point>;
    using ControlNets = VectorT<ControlNet, PVectorDim>;

    using PSpacePoint2D = VectorT<PSpaceUnit_Type, 2>;
    using PlanarPolygon = VectorT<PSpacePoint2D, PVectorDim>;

    // Constructor(s)
    template <typename... Ts>
    PGeneralizedBezierPatch(const ControlNets& cns, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_CNs{cns}, m_l{int(m_CNs.at(0).rows())},
        m_d{int(m_CNs.at(0).rows() + m_CNs.at(0).columns()) - 1}
    {

      assert(cns.size() == PVectorDim);
      if (cns.size() != PVectorDim)
        throw std::runtime_error("Wrong dimension: Control Nets");

      initContructRegularPolygon();
      initConstructRibbons();
    }

    template <typename... Ts>
    PGeneralizedBezierPatch(const ControlNets& cns, const PlanarPolygon& P,
                            Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_CNs{cns}, m_l{int(m_CNs.at(0).rows())},
        m_d{int(m_CNs.at(0).rows() + m_CNs.at(0).columns()) - 1}, m_P{P}
    {
      assert(cns.size() == PVectorDim);
      if (cns.size() != PVectorDim)
        throw std::runtime_error("Wrong dimension: Control Nets");

      initConstructRibbons();
    }

  private:
    void initConstructRibbons()
    {
      // Check control net sizes
      int CN_col_size = int(m_CNs.at(0).columns());
      int CN_row_size = int(m_CNs.at(0).rows());

      // col size > 2
      const auto COL_SIZE_CHECK = CN_col_size >= 2;
      assert(COL_SIZE_CHECK);
      if (not COL_SIZE_CHECK)
        throw std::runtime_error{"Needs degree 3; |CN col| < 2!"};

      // row size == col size || row size == (col_size - 1)
      const auto ROW_COL_SIZE_CHECK
        = (CN_row_size == CN_col_size or CN_row_size == (CN_col_size - 1));
      assert(ROW_COL_SIZE_CHECK);
      if (not ROW_COL_SIZE_CHECK)
        throw std::runtime_error{"|CN row| != {|CN col|,|CN col -1|}!"};

      // Equivalent sized rows and cols;
      for (const auto& CN : m_CNs) {
        const auto EQUIVE_SIZE_CHECK
          = int(CN.columns()) == CN_col_size and int(CN.rows()) == CN_row_size;
        assert(EQUIVE_SIZE_CHECK);
        if (not EQUIVE_SIZE_CHECK)
          throw std::runtime_error{
            "|CN_j row| != |CN_0 row| or |CN_j col| != |CN_0 col|!"};
      }



      // Construct ribbons
      constexpr int N = PVectorDim;
      for (int j = 0; j < N; ++j) {

        auto i  = j;
        auto im = (j + N - 1) % N;

        auto& CNi  = m_CNs.at(size_t(i));
        auto& CNim = m_CNs.at(size_t(im));

        auto& C = m_C.at(size_t(i));

        // resize C
        C.resize(CNim.rows(), CNim.columns() + CNi.rows());

        // fill C[i] from CN[i-1]
        for (auto c_i = 0UL; c_i < CNim.rows(); ++c_i)
          for (auto c_j = 0UL; c_j < CNim.columns(); ++c_j)
            C.at(c_i, c_j) = CNim.at(c_i, c_j);

        // fill C[i] from CN[i]
        for (auto c_i = 0UL; c_i < CNim.rows(); ++c_i)
          for (auto c_j = 0UL; c_j < CNi.rows(); ++c_j)
            C.at(c_i, (CNim.columns() + c_j))
              = CNi.at(CNi.rows() - 1 - c_j, c_i);
      }
    }

    void initContructRegularPolygon()
    {
      m_P = utils::parametric::generateRegularPolygon2D<PVectorDim, Unit_Type,
                                                        PSpaceUnit_Type>();
    }


  public:
    ControlNets m_CNs{};
    ControlNets m_C{};

    int m_i{0};
    int m_l{0};   // Equals to CNs_i rows; Layers
    int m_d{0};   // Equals to CNs_i colums + CNs_i rows - 1; Degree


    VectorT<PSpacePoint2D, PVectorDim> m_P;

    // TPSurf interface
  public:
    PBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PBoolArray, PVectorDim>(false);
    }
    PSpacePoint startParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PVectorDim>(0.0);
    }
    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PVectorDim>(1.0);
    }

  protected:
    EvaluationResult evaluate(
      const PSpacePoint& par,
      const PSizeArray& /*no_der*/
      = utils::initStaticContainer<PSizeArray, PVectorDim>(1UL),
      const PBoolArray& /*from_left*/
      = utils::initStaticContainer<PBoolArray, PVectorDim>(true)) const override
    {

      // Find MVC
      const auto gbc = utils::gbc::mvc(m_P, blaze::inner(par, m_P));

      // RRRRRRRRRotated parameters
      VectorT<PSpacePoint, PVectorDim>         Lambdas;
      VectorT<Unit_Type, PVectorDim>           Sis;
      VectorT<Unit_Type, PVectorDim>           His;
//      VectorT<Unit_Type, PVectorDim>           Hims;
//      VectorT<Unit_Type, PVectorDim>           Hips;
      VectorT<DMatrixT<Unit_Type>, PVectorDim> Bss;
      VectorT<DMatrixT<Unit_Type>, PVectorDim> Bhs;
      VectorT<DMatrixT<Unit_Type>, PVectorDim> Mus;

      // Lambdas
      for (size_t i = 0; i < PVectorDim; ++i)
        Lambdas[i] = detail::rotateLambda(gbc, m_i + int(i));

      // Local coords: s/h-parameterization
      for (size_t i = 0; i < PVectorDim; ++i) {

        // i-s
        const size_t n  = PVectorDim;
        const size_t ic = (n + i) % n;

        // Local coords
        Sis[i]  = utils::gbc::gbcToSMapping(m_P, Lambdas[ic]);
        His[i]  = utils::gbc::gbcToHMapping(Lambdas[ic]);
      }

      // Pre-computation - Basis function and deficiency weights
      for (size_t i = 0; i < PVectorDim; ++i) {

        // i-s
        const size_t n  = PVectorDim;
        const size_t im = (n + i - 1) % n;
        const size_t ip = (n + i + 1) % n;

        // Bs-s and Bh-s
        Bss[i] = basis::generateBernsteinBasisMatrix(m_d, Sis[i]);
        Bhs[i] = blaze::evaluate(
          blaze::submatrix(basis::generateBernsteinBasisMatrix(m_d, His[i]),
                           0UL, 0UL, size_t(m_l), size_t(m_l)));

        // Mu-s
        Mus[i] = generateMuMatrix(His[im], His[i], His[ip]);
      }




      // Evaluation result (point and normal)
      EvaluationResult p;
      if constexpr(VectorDim == 3)
        p.resize(3);
      else
        p.resize(1);

      const auto       C0 = computeC0();



      // Position
      Point Si_sum = C0;
      Vector DsSi_sum = C0;
      Vector DhSi_sum = C0;

      // Per-side evaluation
      for (size_t i = 0; i < PVectorDim; ++i) {

        const auto Bhs_i = Bhs[i];
        const auto Bss_i = blaze::trans(Bss[i]);
        const auto Mus_i = Mus[i];
        const auto C_i   = m_C[i];

        // Edge-ribbon evaluation
        Si_sum += (Bhs_i * ((Mus_i % C_i) * Bss_i))(0, 0);
        DsSi_sum += (Bhs_i * ((Mus_i % C_i) * Bss_i))(1, 0);
        DhSi_sum += (Bhs_i * ((Mus_i % C_i) * Bss_i))(0, 1);

        // Center point correction
        Si_sum -= C0 * (Bhs_i * (Mus_i * Bss_i))(0, 0);
        DsSi_sum -= C0 * (Bhs_i * (Mus_i * Bss_i))(1, 0);
        DhSi_sum -= C0 * (Bhs_i * (Mus_i * Bss_i))(0, 1);
      }

      // Convert to homogenous coordinates
      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Si_sum, 1.0);

      if constexpr(VectorDim == 3) {
        p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          DsSi_sum, 0.0);
        p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          DhSi_sum, 0.0);
      }

      return p;
    }


  private:
    Unit_Type compute_mu(int j, int k, const PSpaceUnit_Type& him,
                         const PSpaceUnit_Type& hi,
                         const PSpaceUnit_Type& hip) const
    {
      if (k < 2) {
        if (2 <= j and j <= (m_d - 2))
          return Unit_Type(1);
        else if (j < 2) {
          if (std::abs(hi + him) < 1e-7) {
            return Unit_Type(0.5);
          }
          return him / (him + hi);
        }
        else {   // j > d - 2
          if (std::abs(hi + hip) < 1e-7) {
            return Unit_Type(0.5);
          }
          return hip / (hip + hi);
        }
      }
      else {   //( k >= 3)
        if (j < m_l) {
          if (j < k)
            return Unit_Type(0);
          else if (j == k)
            return Unit_Type(0.5);
          else   // (j>k)
            return Unit_Type(1);
        }
        else {   // j >= l
          if (j > m_d - k)
            return Unit_Type(0);
          else if (j == m_d - k)
            return Unit_Type(0.5);
          else
            return Unit_Type(1);   // (j< d-k)
        }
      }
    }

    DMatrixT<Unit_Type> generateMuMatrix(const PSpaceUnit_Type& him,
                                       const PSpaceUnit_Type& hi,
                                       const PSpaceUnit_Type& hip) const
    {
      DMatrixT<Unit_Type> Mu(size_t(m_l), size_t(m_d + 1));
      for (size_t k = 0; k < Mu.rows(); ++k)
        for (size_t j = 0; j < Mu.columns(); ++j)
          Mu(k, j) = compute_mu(int(j), int(k), him, hi, hip);
      return Mu;
    }

public:
    Point computeC0() const
    {
      Point C0_sum(0);
      for (size_t i = 0; i < PVectorDim; ++i)
        C0_sum += m_C[i](size_t(m_l - 1), size_t(m_l));

      return (Unit_Type(1) / Unit_Type(PVectorDim)) * C0_sum;
    }


  };   // class PGeneralizedBezierPatch





}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_STUFF_POLYGONS_PGENERALIZEDBEZIERPATCH_H
