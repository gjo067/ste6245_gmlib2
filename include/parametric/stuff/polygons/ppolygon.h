#ifndef GM2_PARAMETRIC_STUFF_POLYGONS_PPOLYGON_H
#define GM2_PARAMETRIC_STUFF_POLYGONS_PPOLYGON_H

#include "../../parametricobject.h"
#include "../../../core/utils.h"

namespace gmlib2::parametric
{

  template <size_t ParameterSpaceVectorDim_T,
            typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            size_t ParameterSpaceFrameDim_T = ParameterSpaceVectorDim_T>
  class PPolygon
    : public PPolygonSurface<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                             ParameterSpaceFrameDim_T> {
    using Base
      = PPolygonSurface<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                        ParameterSpaceFrameDim_T>;

  public:
    // Inherited Types
    using Unit_Type                 = typename Base::Unit_Type;
    using EvaluationResult          = typename Base::EvaluationResult;
    using Point                     = typename Base::Point_Type;
    using Vector                    = typename Base::Vector_Type;
    using VectorH                   = typename Base::VectorH_Type;
    static constexpr auto VectorDim = Base::VectorDim;

    // Inherited Parameter space types
    static constexpr auto PVectorDim = Base::PSpace_VectorDim;
    using PSpacePoint                = typename Base::PSpace_Point_Type;
    using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
    using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

    // My types
    using ControlPoints = VectorT<Point, PVectorDim>;
    using Triangle = std::array<size_t, 3>;

    enum class GbcMethod { Barycentric, MeanValue3DMapping };

    // Constructor(s)
    template <typename... Ts>
    PPolygon(const ControlPoints& cps, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_cps(cps)
    {
    }

    GbcMethod gbcMethod() const;
    void      setGbcMethod(GbcMethod method);
    const ControlPoints& controlPoints() const;

  protected:
    virtual PSpacePoint gbc(const PSpacePoint& par) const;

  private:
    PSpacePoint              meanValue3DMapping(const PSpacePoint& par) const;
    bool                     isTriangulated() const { return m_triangles.size(); }
    void                     triangulate() const;
    virtual EvaluationResult do_evaluate(
      const PSpacePoint& par,
      const PSizeArray&  no_der
      = utils::initStaticContainer<PSizeArray, PVectorDim>(0UL),
      const PBoolArray& from_left
      = utils::initStaticContainer<PBoolArray, PVectorDim>(true)) const;

    // Member(s)
  private:
    ControlPoints                 m_cps;
    mutable std::vector<Triangle> m_triangles;
    GbcMethod                     m_gbc{GbcMethod::Barycentric};

    // PPolygon interface
  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSizeArray& no_der,
             const PBoolArray& from_left
             = utils::initStaticContainer<PBoolArray, PVectorDim>(true))
      const override;
    PBoolArray  isClosed() const override;
    PSpacePoint startParameters() const override;
    PSpacePoint endParameters() const override;
  };


  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::PBoolArray
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::isClosed() const
  {
    return utils::initStaticContainer<PBoolArray, PVectorDim>(false);
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::PSpacePoint
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::startParameters() const
  {

    return utils::initStaticContainer<PSpacePoint, PVectorDim>(0.0);
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::PSpacePoint
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::endParameters() const
  {
    return utils::initStaticContainer<PSpacePoint, PVectorDim>(1.0);
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::GbcMethod
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::gbcMethod() const
  {
    return m_gbc;
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  void PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                ParameterSpaceFrameDim_T>::setGbcMethod(GbcMethod method)
  {
    m_gbc = method;
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  const typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                          ParameterSpaceFrameDim_T>::ControlPoints&
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::controlPoints() const
  {
    return m_cps;
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::PSpacePoint
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::gbc(const PSpacePoint& par) const
  {
    switch (m_gbc) {
      case GbcMethod::Barycentric:
        return par;
      case GbcMethod::MeanValue3DMapping:
        return meanValue3DMapping(par);
    }
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::EvaluationResult
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::evaluate(const PSpacePoint& par,
                                               const PSizeArray&  no_der,
                                               const PBoolArray&  from_left)
    const
  {

//    std::cout << " -- Polygon eval BEGIN --" << std::endl;
//    std::cout << "    --  GBC --" << std::endl;
    auto gbc_par = gbc(par);

//    std::cout << "    --  GBC END --" << std::endl;
//    std::cout << "    --------------" << std::endl;
//    std::cout << "    par: \n" << par << std::endl;
//    std::cout << "    gbc_par: \n" << gbc_par << std::endl;
//    std::cout << " -- Polygon eval END --" << std::endl;
//    std::cout << std::endl << std::endl;

    return do_evaluate(gbc_par, no_der, from_left);
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::EvaluationResult
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::
    do_evaluate(const PSpacePoint& par, const PSizeArray& /*no_der*/,
                const PBoolArray& /*from_left*/) const
  {
    EvaluationResult p(1);
    p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
      blaze::inner(par, m_cps), 1.0);

    //    std::cout << "----- Polygon eval -----" <<std::endl;
    //    std::cout << "  par: " << std::endl << par << std::endl;
    //    std::cout << "  p[0]: " << std::endl << p[0] << std::endl;
    //    std::cout << "  m_cps: " << std::endl << m_cps << std::endl;
    //    std::cout << std::endl;

    return p;
  }

  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  void PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                ParameterSpaceFrameDim_T>::triangulate() const
  {
    m_triangles.clear();

    auto indPM = [](const auto& ind_in, const auto& len_in) {
      const int ind = int(ind_in);
      const int len = int(len_in);
      const int im  = (ind + (len - 1)) % len;
      const int i   = (ind) % len;
      const int ip  = (ind + 1) % len;

      return std::make_tuple(size_t(im), size_t(i), size_t(ip));
    };

    auto findProtrudingPointAndCreateTriangle
      = [&](const auto& cpi, const auto& cpts, auto& tris) {
          const auto nn = int(cpi.size());
          assert(nn > 2);

          for (int k = 0; k < nn; ++k) {
            const auto [im, i, ip] = indPM(k, nn);
            const auto co
              = blaze::inner(cpts[size_t(cpi[size_t(im)])], cpts[size_t(cpi[size_t(ip)])])
                / (blaze::length(cpts[size_t(cpi[size_t(im)])]) * blaze::length(cpts[size_t(cpi[size_t(ip)])]));
            const auto ang = blaze::acos(co);

            if (std::abs(ang) < M_PI) {
              tris.push_back(Triangle{size_t(cpi[size_t(im)]), size_t(cpi[size_t(i)]), size_t(cpi[size_t(ip)])});
              return k;
            }
          }

          throw std::runtime_error("PPolygon::triangulate() -> lambda::findProdtrudingPointAndCreateTriangle -- unexpected condition!");
          assert(false);
        };

    // std::vector<size_t> cpi, std::vector<Triangles> triangles
    auto createTriangle = [&](auto& cpi, const auto& cpts, auto& tris) {
      auto ind_to_remove
        = findProtrudingPointAndCreateTriangle(cpi, cpts, tris);

      // exclude p_i from polygon
      cpi.erase(cpi.begin() + ind_to_remove);

      // Continue?
      return cpi.size() > 2;
    };

    // Control point indices
    std::vector<int> cp_indices;
    for (int k = 0; k < int(m_cps.size()); ++k) cp_indices.push_back(k);

    // Triangulate by protruding point removal
    while (createTriangle(cp_indices, m_cps, m_triangles)) {
    }
  }


  template <size_t ParameterSpaceVectorDim_T, typename SpaceObjectEmbedBase_T,
            size_t ParameterSpaceFrameDim_T>
  typename PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
                    ParameterSpaceFrameDim_T>::PSpacePoint
  PPolygon<ParameterSpaceVectorDim_T, SpaceObjectEmbedBase_T,
           ParameterSpaceFrameDim_T>::meanValue3DMapping(const PSpacePoint&
                                                           par_in) const
  {
    if (not isTriangulated()) triangulate();

    const auto n = par_in.size();
    const auto x
      = blaze::evaluate(blaze::subvector(do_evaluate(par_in)[0], 0UL, 3UL));

    const auto epsilon = 1e-6;

    // Construct principle polygon vertex-parameters
    PSpacePoint par_p(Unit_Type(0.0));
    par_p[0] = Unit_Type(1.0);

    // Cache containers
    DVectorT<Unit_Type>   d(n);
    DVectorT<Vector>      p(n);
    DVectorT<Vector>      u(n);
    DVectorT<PSpacePoint> f(n);


    auto indPM = [](const auto& ind_in, const auto& len_in) {

      const int ind = int(ind_in);
      const int len = int(len_in);


      const int im = (ind + (len - 1)) % len;
      const int i  = (ind) % len;
      const int ip = (ind + 1) % len;

      return std::make_tuple(size_t(im), size_t(i), size_t(ip));
    };


    // Sample "mesh" of surrounding "polyhedron" -- PRECOMPUTATIONS
    for (size_t j = 0; j < n; ++j) {
      f[j]           = par_p;
      p[j]           = blaze::subvector(do_evaluate(f[j])[0], 0UL, 3UL);
      const auto pmx = p[j] - x;
      d[j]           = blaze::length(pmx);

      if (d[j] < epsilon) {
//        std::cout << " -- In polygon vertex!" << std::endl;
        return f[j];
      }

      u[j] = blaze::evaluate(pmx / d[j]);
      std::rotate(par_p.begin(), par_p.end() - 1, par_p.end());
    }
    //    // -- Debug
    //    for (size_t i = 0; i < n; ++i) {
    //      std::cout << " d[i]: " << d[i] << std::endl;
    //    }
    //    // -- Debug END


    PSpacePoint totalF{0};
    Unit_Type   totalW{0};

//    std::vector<Triangle> triangles; {
//      triangles.push_back(Triangle {0, 1, 3});
//      triangles.push_back(Triangle {1, 2, 3});
//      triangles.push_back(Triangle {0, 3, 4});
//    }

    // For each triangle with vertices p and values f
    for (Triangle& tri : m_triangles) {
      VectorT<Unit_Type, 3> theta(0);

      for (size_t k = 0; k < 3; ++k) {
        const auto im = tri[0];
        [[maybe_unused]] const auto i = tri[1];
        const auto ip = tri[2];
        //        std::cout << "Theta comp [j,k,im,i,ip]: " << j << "," << k <<
        //        "," << im
        //                  << "," << i << "," << ip << std::endl;

        const auto l_k = blaze::length(u[ip] - u[im]);
        theta[k] = Unit_Type(2) * std::asin(l_k / Unit_Type(2));
        //        std::cout << "Vinkel: theta[" << k << "]: " << theta[k] / M_PI
        //        << std::endl; std::cout << "Vectorer: " << std::endl << u[im]
        //        <<
        //                     "          " << std::endl << u[i] <<
        //                     "          " << std::endl << u[ip] << std::endl;
        assert(l_k <= 2);
        assert(blaze::length(u[i]) - 1 < epsilon);

        std::rotate(tri.begin(), tri.end() - 1, tri.end());
      }

      const auto theta_sum
        = std::accumulate(theta.begin(), theta.end(), Unit_Type(0));
      const auto h = theta_sum / Unit_Type(2);


      // If x lies on tri, use 2D MVC
      if (std::abs(M_PI - h) < epsilon) {
        PSpacePoint wf = PSpacePoint{0};
        Unit_Type   w     = 0;

        for (size_t k = 0; k < 3; ++k) {
          const auto im = tri[0];
          const auto i  = tri[1];
          const auto ip = tri[2];
//          std::cout << "2D MVC comp [j,k,im,i,ip]: " << j << "," << k << ","
//                    << im << "," << i << "," << ip << std::endl;

          const auto wk  = std::sin(theta[k]) * d[im] * d[ip];
          const auto wfk = wk * f[i];

          wf += wfk;
          w += wk;

          std::rotate(tri.begin(), tri.end() - 1, tri.end());
        }

//        std::cout << " -- On boundary... 2D Mean value!" << std::endl;
        return wf / w;
      }


      VectorT<Unit_Type,3>   c(0);
      VectorT<Unit_Type,3>   sq(0);
      VectorT<Unit_Type,3>   s(0);

      for (size_t k = 0; k < 3; ++k) {
        const auto im = tri[0];
        const auto i  = tri[1];
        const auto ip = tri[2];
        const auto [km, ki, kp] = indPM(k,3);
//        std::cout << "c/s comp [j,k,im,i,ip,km,ki,kp]: " << j << "," << k
//                  << "," << im << "," << i << "," << ip << "," << km << ","
//                  << ki << "," << kp << std::endl;

        assert(std::abs(h) <= M_PI);

        const auto c_k_num
          = Unit_Type(2) * std::sin(h) * std::sin(h - theta[ki]);
        const auto c_k_denum = std::sin(theta[kp]) * std::sin(theta[km]);

        // cos(\gamma_i)
        c[k] = (c_k_num / c_k_denum) - Unit_Type(1);
        //        std::cout << "    -- c[" << k << "]: " << c[k] << " => " <<
        //        c_k_num << " / " << c_k_denum << std::endl;

        using SMatrixT
          = blaze::StaticMatrix<Unit_Type, 3UL, 3UL, blaze::rowMajor>;

        SMatrixT U; {
          column(U, 0UL) = u[im];
          column(U, 1UL) = u[i];
          column(U, 2UL) = u[ip];
        }
        auto det_u = blaze::det(U);
//        std::cout << "    -- det: " << det_u << std::endl;

        Unit_Type sgn{1};
        if (det_u < 0) sgn = Unit_Type{-1};

        sq[k] = Unit_Type(1) - std::pow(c[k], 2.0);
        if (std::abs(sq[k]) < epsilon) sq[k] = 0;

        assert(sq[k] >= 0);
        assert(sq[k] <= 1.0);

        // Sin(\gamma_i)
        s[k] = sgn * std::sqrt(sq[k]);
//        std::cout << "    -- sq_k[" << k << "]: " << sq[k] << std::endl;
//        std::cout << "    -- s_k[" << k << "]: " << s[k] << std::endl;
//        std::cout << std::endl;

        std::rotate(tri.begin(), tri.end() - 1, tri.end());
      }

//      std::cout << "  -- s: " << std::endl << s << std::endl;
//      std::cout << std::endl;

      auto sk_existence = [&epsilon](const auto& s_k){
        return std::abs(s_k) <= epsilon;
      };

      // If the point x lies outside tri on the same plane as tri, ignore tri
      if (sk_existence(s[0]) or sk_existence(s[1]) or sk_existence(s[2])) {
        continue;
      }


      PSpacePoint wf = PSpacePoint{0};
      Unit_Type   w  = 0;

      for (size_t k = 0; k < 3; ++k) {
        const auto i            = tri[1];
        const auto [km, ki, kp] = indPM(k, 3);
        //        std::cout << "3D MVC comp [j,k,im,i,ip,km,ki,kp]: " << j
        //        << "," << k
        //                  << "," << im << "," << i << "," << ip << "," <<
        //                  km << ","
        //                  << ki << "," << kp << std::endl;

        const auto wk_num = (theta[k] - c[kp] * theta[km] - c[km] * theta[kp]);
        const auto wk_denum = d[i] * std::sin(theta[kp]) * s[km];
        const auto wk       = wk_num / wk_denum;
        const auto wfk      = wk * f[i];

        wf += wfk;
        w += wk;

        std::rotate(tri.begin(), tri.end() - 1, tri.end());
      }

      totalF += wf;
      totalW += w;
//      std::cout << "  ** inside -- contribute" << std::endl;
    }

    return totalF / totalW;
  }

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_STUFF_POLYGONS_PPOLYGON_H
