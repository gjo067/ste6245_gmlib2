#ifndef GM2_PARAMETRIC_STUFF_GBCUTILS_H
#define GM2_PARAMETRIC_STUFF_GBCUTILS_H










/////////////
// Algorithms


namespace gmlib2::utils::gbc
{

  namespace detail
  {

  }   // namespace detail







  /**!
   * Mean value coordiantes for Arbitrary Planar Polygons, Hormann and Floater
   * -- Only for simple polygons (does not contain other polygons) --
   *
   * \tparam UnitType_T UnitType - real type: (float,double)
   * \tparam Point_T Polygon Point type - euclidean type
   * \tparam M_T Nr. of sides in the polygon
   * \tparam VD_T Dimension of the Vector-space of the planar polygon
   *
   * \param[in] V An arbitrary Planar Polygon
   * \param[in] x A vertex point inside the polygon
   * \param[in] F Per vertex; v in V, associated data points
   *
   * \returns the mean value weights lambda_i, i=0,...,n-1
   */
  template <typename Unit_Type_T, size_t M_T, size_t VectorDim_T,
            size_t VectorDimF_T>
  inline auto mvc(const VectorT<VectorT<Unit_Type_T, VectorDim_T>, M_T>&  V,
                  const VectorT<Unit_Type_T, VectorDim_T>&                x,
                  const VectorT<VectorT<Unit_Type_T, VectorDimF_T>, M_T>& F)
  {
    using UnitType = Unit_Type_T;
    using Vector   = VectorT<UnitType, VectorDim_T>;

    constexpr auto N  = int(M_T);   // PVectorDim
    using PSpacePoint = VectorT<UnitType, VectorDimF_T>;
    using ReturnType  = const PSpacePoint;


    VectorT<Vector, N> s;
    std::transform(std::begin(V), std::end(V), std::begin(s),
                   [&x](const auto& v_i) { return v_i - x; });

    VectorT<UnitType, N> r;
    VectorT<UnitType, N> A;
    VectorT<UnitType, N> D;
    for (int i = 0; i < N; ++i) {

      const auto ip1 = (i + N + 1) % N;
      r[size_t(i)]   = blaze::l2Norm(s[size_t(i)]);

      MatrixT<UnitType, 2, 2> A_mat;
      blaze::column(A_mat, 0UL) = s[size_t(i)];
      blaze::column(A_mat, 1UL) = s[size_t(ip1)];

      A[size_t(i)] = blaze::det(A_mat) / UnitType(2);
      D[size_t(i)] = blaze::inner(s[size_t(i)], s[size_t(ip1)]);

      // x == v_i
      if (std::abs(r[size_t(i)]) < 1e-7) return ReturnType(F[size_t(i)]);

      // x \in e_i (v_i,v_{i+1})
      if (std::abs(A[size_t(i)]) < 1e-7 and D[size_t(i)] < 0) {

        r[size_t(ip1)] = blaze::l2Norm(s[size_t(ip1)]);

        return ReturnType(
          ((r[size_t(ip1)] * F[size_t(i)]) + (r[size_t(i)] * F[size_t(ip1)]))
          / (r[size_t(i)] + r[size_t(ip1)]));
      }
    }

    PSpacePoint f(0);
    UnitType    W = 0;
    for (int i = 0; i < N; ++i) {

      const auto ip1 = (i + N + 1) % N;
      const auto im1 = (i + N - 1) % N;

      UnitType w = 0;

      if (std::abs(A[size_t(im1)]) >= 1e-7)
        w += (r[size_t(im1)] - (D[size_t(im1)] / r[size_t(i)]))
             / A[size_t(im1)];

      if (std::abs(A[size_t(i)]) >= 1e-7)
        w += (r[size_t(ip1)] - (D[size_t(i)] / r[size_t(i)])) / A[size_t(i)];

      f += w * F[size_t(i)];
      W += w;
    }

    const auto f_over_W = f / W;

    return ReturnType(f_over_W);
  }


  template <typename Unit_Type_T, size_t M_T, size_t VectorDim_T>
  inline auto mvc(const VectorT<VectorT<Unit_Type_T, VectorDim_T>, M_T>& V,
                  const VectorT<Unit_Type_T, VectorDim_T>&               x)
  {
    using PSpacePoint = VectorT<Unit_Type_T, M_T>;

    auto F = VectorT<PSpacePoint, M_T>(PSpacePoint(0));
    for (size_t i = 0; i < M_T; ++i) F[i][i] = 1;

    return mvc(V, x, F);
  }


  template <typename Unit_Type_T, size_t M_T, size_t VectorDim_T>
  inline auto
  mvcFromGbc(const VectorT<VectorT<Unit_Type_T, VectorDim_T>, M_T>& V,
             const VectorT<Unit_Type_T, M_T>&                       gbc_lambda)
  {
    return mvc(V, blaze::inner(V, gbc_lambda));
  }


  template <typename Unit_Type_T, size_t M_T>
  inline auto
  compositMvcMapping(const VectorT<VectorT<Unit_Type_T, 2>, M_T>& V_s,
                     const VectorT<VectorT<Unit_Type_T, 2>, M_T>& V_t,
                     const VectorT<Unit_Type_T, 2>& v, int steps)
  {
    // Types
    using UnitType   = Unit_Type_T;
    constexpr auto N = int(M_T);
    using Point      = VectorT<Unit_Type_T, 2>;
    using Polygon    = VectorT<Point, N>;

    // Construct range
    const auto            dt = 1.0 / UnitType(steps - 1);
    int                   i  = 0;
    std::vector<UnitType> range((size_t(steps - 1)));
    for (auto& t : range) t = dt * UnitType(++i);

    // Compute weights
    auto phi = mvc(V_s, v);

    // Do composite mapping
    Point   x      = v;
    Polygon P      = V_s;
    Polygon P_prev = P;
    for (const auto& t : range) {

      // Linear point curve function : P(t_{j+1})
      std::transform(std::begin(V_s), std::end(V_s), std::begin(V_t),
                     std::begin(P), [&t](const auto& v_s, const auto& v_t) {
                       // return v_s + t * (v_t - v_s);
                       return (1 - t) * v_s + t * v_t;
                     });

      // Compute step
      Polygon S = P;
      std::transform(
        std::begin(P_prev), std::end(P_prev), std::begin(P), std::begin(S),
        [](const auto& v_prev, const auto& v_p) { return v_p - v_prev; });

      // g_j(x), j = 0, ... , m-2
      x += blaze::inner(S, phi);
      P_prev = P;
    }

    // MVC phi_i; i = j = m-1
    // return mvc(V_t,x);
    return x;
  }






  template <typename Unit_Type_T, size_t N_T>
  inline auto mvcToShMapping(const VectorT<VectorT<Unit_Type_T, 2>, N_T>& V,
                             const VectorT<Unit_Type_T, N_T>& lambda, int i = 0,
                             int j = 0)
  {

    // Rotate polygon index
    auto rotate_i
      = [&i, &j](int k) { return size_t((int(N_T) + i + j + k) % int(N_T)); };


    // Angle of Vector [jm,j,jp] in V
    auto compute_theta = [](const auto& vjm, const auto& vj, const auto& vjp) {
      const auto a = vjm - vj;
      const auto b = vjp - vj;

      const auto co
        = blaze::inner(a, b) / (blaze::l2Norm(a) * blaze::l2Norm(b));
      return blaze::acos(co);
    };

    // Compute the perpendicular distance from v onto the line [vi,vim1]
    auto compute_perp_dist
      = [](const auto& vi, const auto& vim1, const auto& v) {
          const auto vj
            = blaze::l2Norm(vi - v) >= 1e-7 ? vi : vi + 0.1 * (vi - vim1);
          const auto vjm1
            = blaze::l2Norm(vim1 - v) >= 1e-7 ? vim1 : vi - 0.1 * (vi - vim1);

          const auto a = vj - vjm1;
          const auto b = v - vjm1;

          const auto c
            = (a * blaze::inner(a, b)) / (blaze::length(a) * blaze::length(a));

          const auto d = b - c;

          return blaze::l2Norm(d);
        };


    // Indices
    const auto idxm2 = rotate_i(-2);
    const auto idxm1 = rotate_i(-1);
    const auto idx   = rotate_i(0);
    const auto idxp1 = rotate_i(1);

    // Vertices
    const auto v    = blaze::inner(V, lambda);
    const auto vim2 = V.at(idxm2);
    const auto vim1 = V.at(idxm1);
    const auto vi   = V.at(idx);
    const auto vip1 = V.at(idxp1);

    // compute side-coordinate (s or u)
    const auto stim1 = std::sin(compute_theta(vim2, vim1, vi));
    const auto sti   = std::sin(compute_theta(vim1, vi, vip1));

    const auto him1 = compute_perp_dist(vim2, vim1, v);
    const auto hip1 = compute_perp_dist(vip1, vi, v);

    const auto s = (sti * him1) / ((sti * him1) + (stim1 * hip1));

    // compute distance-coordinate (h or v)
    const auto h = Unit_Type_T(1) - lambda[idxm1] - lambda[idx];

    return std::pair(s, h);
  }


  template <typename Unit_Type_T, size_t N_T>
  inline auto mvcToShMapping(const VectorT<VectorT<Unit_Type_T, 2>, N_T>& V,
                             const VectorT<Unit_Type_T, 2>& x, int i = 0,
                             int j = 0)
  {
    mvcToShMapping(V, mvc(V, x), i, j);
  }


  template <typename Unit_Type_T, size_t N_T>
  inline auto gbcToSMapping(const VectorT<VectorT<Unit_Type_T, 2>, N_T>& V,
                            const VectorT<Unit_Type_T, N_T>&             lambda)
  {
    // Rotate polygon index
    auto rotate_idx = [](int k) { return size_t((int(N_T) + k) % int(N_T)); };

    // Angle of Vector [jm,j,jp] in V
    auto compute_theta = [](const auto& vjm, const auto& vj, const auto& vjp) {
      const auto a = vjm - vj;
      const auto b = vjp - vj;

      const auto co
        = blaze::inner(a, b) / (blaze::l2Norm(a) * blaze::l2Norm(b));
      return blaze::acos(co);
    };

    // Compute the perpendicular distance from v onto the line [vi,vim1]
    auto compute_perp_dist = [](const auto& vi, const auto& vim1,
                                const auto& v) {
      const auto vj = vi;
      const auto vjm1
        = blaze::l2Norm(v - vim1) >= 1e-5 ? vim1 : vim1 - (0.1 * (vi - vim1));

      const auto a = vj - vjm1;
      const auto b = v - vjm1;

      const auto c
        = (a * blaze::inner(a, b)) / (blaze::length(a) * blaze::length(a));
      //          const auto c
      //            = (a * blaze::inner(a, b)) / (blaze::length(a) *
      //            blaze::length(a));

      const auto d = b - c;

      const auto l2N_d = blaze::l2Norm(d);

      //          std::cout << "a:\n"
      //                    << a << ", b:\n"
      //                    << b << ", c:\n"
      //                    << c << ", d:\n"
      //                    << d << ", ||d||: " << l2N_d << std::endl;

      return l2N_d;
    };


    // Indices
    const auto im2 = rotate_idx(-2);
    const auto im1 = rotate_idx(-1);
    const auto i   = rotate_idx(0);
    const auto ip1 = rotate_idx(1);

    // Vertices
    const auto v    = blaze::inner(V, lambda);
    const auto vim2 = V.at(im2);
    const auto vim1 = V.at(im1);
    const auto vi   = V.at(i);
    const auto vip1 = V.at(ip1);
    //    std::cout << "v:\n"
    //              << v << ", vim2:\n"
    //              << vim2 << ", vim1:\n"
    //              << vim1 << ", vi:\n"
    //              << vi << ", vip1:\n"
    //              << vip1 << std::endl;

    // compute side-coordinate (s or u)
    const auto stim = std::sin(compute_theta(vim2, vim1, vi));
    const auto sti  = std::sin(compute_theta(vim1, vi, vip1));

    //    std::cout << "------------------- == him" << std::endl;
    const auto him = compute_perp_dist(vim2, vim1, v);

    //    std::cout << "------------------- == hip" << std::endl;
    const auto hip = compute_perp_dist(vip1, vi, v);

    //    std::cout << "------------------ him: " << him << std::endl;
    //    std::cout << "------------------ hip: " << hip << std::endl;

    const auto s = (sti * him) / ((sti * him) + (stim * hip));



    //    std::cout << " gbcToSMapping:";
    //    std::cout << ", lambda: ";
    //    for( const auto& l : lambda ) std::cout << l << " ";
    //    std::cout << ", im2: " << im2 << ", im1: " << im1 << ", i: " << i
    //              << ", ip1: " << ip1;
    //    std::cout << ", s: " << s;
    //    std::cout << std::endl;


    return s;
  }



  template <typename Unit_Type_T, size_t N_T>
  inline auto gbcToHMapping(const VectorT<Unit_Type_T, N_T>& lambda)
  {
    // Rotate polygon index
    auto rotate_idx = [](int k) { return size_t((int(N_T) + k) % int(N_T)); };

    // Indices
    const auto im = rotate_idx(-1);
    const auto i  = rotate_idx(0);

    // compute distance-coordinate (h or v)
    const auto h = Unit_Type_T(1) - lambda[im] - lambda[i];

    //    std::cout << " gbcToHMapping:";
    //    std::cout << ", lambda: ";
    //    for( const auto& l : lambda ) std::cout << l << " ";
    //    std::cout << ", idxm: " << im << ", idx: " << i;
    //    std::cout << ", h: " << h;
    //    std::cout << std::endl;

    return h;
  }




}   // namespace gmlib2::utils::gbc


#endif   // GM2_PARAMETRIC_STUFF_GBCUTILS_H
