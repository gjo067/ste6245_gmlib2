#ifndef GM2_PARAMETRIC_STUFF_UTILS_H
#define GM2_PARAMETRIC_STUFF_UTILS_H






/////////////
// Algorithms

namespace gmlib2::utils::parametric
{

  namespace detail
  {

  }   // namespace detail


  template <size_t N_T, typename Unit_Type_T = double,
            typename PSpaceUnit_Type_T = Unit_Type_T>
  inline auto generateRegularPolygon2D()
  {
    constexpr int N = N_T;

    using Unit_Type = Unit_Type_T;
    using PSpaceUnit_Type = PSpaceUnit_Type_T;
    using Point2D = VectorT<Unit_Type, 2>;
    using Polygon2D = VectorT<Point2D, N>;

    // Construct PSpace mapping polygon
    constexpr auto dt
      = (PSpaceUnit_Type(1) / PSpaceUnit_Type(N)) * PSpaceUnit_Type(2 * M_PI);

    auto circleEval2D = [](const auto t) {
      return std::forward<Point2D>(Point2D{std::cos(t), std::sin(t)});
    };

    Polygon2D P;
    for (int i = 0; i < N; ++i) {
      const auto t      = PSpaceUnit_Type(i) * dt;
      P.at(size_t(i)) = circleEval2D(t);
    }

    using ReturnType = const Polygon2D;
    return std::forward<ReturnType>(P);
  }



}   // namespace gmlib2::utils::parametric


#endif   // GM2_PARAMETRIC_STUFF_UTILS_H
