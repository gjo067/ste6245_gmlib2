#ifndef GM2_CORE_BERNSTEINBASISGENERATORS_H
#define GM2_CORE_BERNSTEINBASISGENERATORS_H

#include "gm2_blaze.h"

namespace gmlib2::basis
{

  template <typename Unit_Type_T>
  inline DMatrixT<Unit_Type_T>
  generateBernsteinBasisMatrix(int d, Unit_Type_T t,
                               Unit_Type_T scale = Unit_Type_T(1))
  {

    // Described on page 91-92 in "Blend book"

    // Initiate result matrix
    DMatrixT<Unit_Type_T> mat(size_t(d + 1), size_t(d + 1));

    // Escape if the degree is 0
    if (d < 1) {
      mat(0, 0) = Unit_Type_T(1);
      return mat;
    }

    // Compute the Bernstein (polynomials), degree 1 -> d, one for each row.
    // Starts from the second bottom row (degree 1), then goes upwards (degree
    // 2,...,d).
    mat(size_t(d - 1), size_t(0)) = 1 - t;
    mat(size_t(d - 1), size_t(1)) = t;

    for (int i = d - 2; i >= 0; i--) {
      mat(size_t(i), size_t(0)) = (1 - t) * mat(size_t(i + 1), size_t(0));
      for (int j = 1; j < d - i; j++)
        mat(size_t(i), size_t(j)) = t * mat(size_t(i + 1), size_t(j - 1))
                                    + (1 - t) * mat(size_t(i + 1), size_t(j));
      mat(size_t(i), size_t(d - i)) = t * mat(size_t(i + 1), size_t(d - i - 1));
    }

    // Compute all the deriatives
    mat(size_t(d), size_t(0)) = -scale;
    mat(size_t(d), size_t(1)) = scale;

    for (int k = 2; k <= d; k++) {
      const double s = k * scale;
      for (int i = d; i > d - k; i--) {
        mat(size_t(i), size_t(k)) = s * mat(size_t(i), size_t(k - 1));
        for (int j = k - 1; j > 0; j--)
          mat(size_t(i), size_t(j))
            = s * (mat(size_t(i), size_t(j - 1)) - mat(size_t(i), size_t(j)));
        mat(size_t(i), size_t(0)) = -s * mat(size_t(i), size_t(0));
      }
    }

    return mat;
  }


}   // namespace gmlib2::basis

#endif   // GM2_CORE_BERNSTEINBASISGENERATORS_H
