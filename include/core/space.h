#ifndef GM2_SPACES_H
#define GM2_SPACES_H


#include "gm2_blaze.h"
#include "traits.h"

//// blaze
//#include <blaze/math/Band.h>

// stl
#include <cmath>
#include <iostream>



namespace gmlib2::spaces
{






  //////////////////////////////////
  ///  Embeddings
  ///
  ///




//  template <typename Unit_T = double, size_t FrameDim_T, size_t VectorDim_T>
//  struct ArbitrarySpaceInfo {
//    using Unit_Type = Unit_T;
//    static constexpr auto FrameDim  = FrameDim_T;
//    static constexpr auto VectorDim = VectorDim_T;
//  };


  template <typename Unit_T = double>
  struct D3R3SpaceInfo {
    using Unit_Type = Unit_T;
    static constexpr auto FrameDim  = 3UL;
    static constexpr auto VectorDim = 3UL;
  };








  //////////////////////////////////
  ///  Vector space
  ///

  template <typename SpaceInfo_T>
  struct VectorSpace {

    // Space_T
    using SpaceInfo_Type            = SpaceInfo_T;
    using Unit_Type                 = typename SpaceInfo_Type::Unit_Type;
    static constexpr auto FrameDim  = SpaceInfo_T::FrameDim;
    static constexpr auto VectorDim = SpaceInfo_T::VectorDim;
    using Vector_Type               = VectorT<Unit_Type, VectorDim>;
    using Frame_Type                = MatrixT<Unit_Type, VectorDim, FrameDim>;
  };



  template <typename SpaceInfo_T>
  struct AffineSpace {

    // "Base" : Vector space
    using VectorSpace_Type = VectorSpace<SpaceInfo_T>;

    // Space_T
    using SpaceInfo_Type = SpaceInfo_T;
    using Unit_Type      = typename SpaceInfo_Type::Unit_Type;

    // Vector space
    static constexpr auto FrameDim  = VectorSpace_Type::FrameDim;
    static constexpr auto VectorDim = VectorSpace_Type::VectorDim;
    using Vector_Type               = typename VectorSpace_Type::Vector_Type;
    using Frame_Type                = typename VectorSpace_Type::Frame_Type;

    // Affine space
    static constexpr auto ASFrameDim = FrameDim + 1;
    using Point_Type                 = VectorT<Unit_Type, VectorDim>;
    using ASFrame_Type = MatrixT<Unit_Type, VectorDim, ASFrameDim>;
  };



  template <typename SpaceInfo_T>
  struct ProjectiveSpace : AffineSpace<SpaceInfo_T> {

    // Base   : Affine Space
    // "Base" : Vector space
    using AffineSpace_Type = AffineSpace<SpaceInfo_T>;
    using VectorSpace_Type = typename AffineSpace_Type::VectorSpace_Type;

    // Space_T
    using SpaceInfo_Type = SpaceInfo_T;
    using Unit_Type      = typename SpaceInfo_Type::Unit_Type;

    // Vector space
    static constexpr auto FrameDim  = VectorSpace_Type::FrameDim;
    static constexpr auto VectorDim = VectorSpace_Type::VectorDim;
    using Vector_Type               = typename VectorSpace_Type::Vector_Type;
    using Frame_Type                = typename VectorSpace_Type::Frame_Type;

    // Affine space
    static constexpr auto ASFrameDim = AffineSpace_Type::ASFrameDim;
    using Point_Type                 = typename AffineSpace_Type::Point_Type;
    using ASFrame_Type               = typename AffineSpace_Type::ASFrame_Type;

    // Projective space
    static constexpr auto VectorHDim = VectorDim + 1;
    using PointH_Type                = VectorT<Unit_Type, VectorHDim>;
    using VectorH_Type               = VectorT<Unit_Type, VectorHDim>;
    using ASFrameH_Type = MatrixT<Unit_Type, VectorHDim, ASFrameDim>;
  };






  namespace vectorspace
  {

    template <typename VectorSpace_T>
    const typename VectorSpace_T::Frame_Type identityFrame()
    {
      using Unit_Type = typename VectorSpace_T::Unit_Type;
      auto FrameDim   = VectorSpace_T::FrameDim;
      auto VectorDim  = VectorSpace_T::VectorDim;
      auto MinDim     = std::min(VectorDim, FrameDim);

      typename VectorSpace_T::Frame_Type I(0);
      for (size_t i = 0; i < MinDim; ++i) I(i, i) = Unit_Type(1);
      //      blaze::diagonal(I) = VectorT<Unit_Type, MinDim>(1); // Next blaze
      return I;
    }
  }   // namespace vectorspace







  namespace affinespace {

    template <typename AffineSpace_T>
    auto
    vSpaceFrame(const typename AffineSpace_T::ASFrameH_Type& frame)
    {
      return blaze::submatrix<0UL, 0UL, AffineSpace_T::VectorDim,
                              AffineSpace_T::ASFrameDim>(frame);
    }
  }   // namespace affinespace




  namespace projectivespace
  {

    template <typename ProjectiveSpace_T>
    const typename ProjectiveSpace_T::ASFrameH_Type identityFrame()
    {
      using Unit_Type = typename ProjectiveSpace_T::Unit_Type;
      auto ASFrameDim  = ProjectiveSpace_T::ASFrameDim;
      auto VectorHDim = ProjectiveSpace_T::VectorHDim;

      typename ProjectiveSpace_T::ASFrameH_Type I(Unit_Type(0));
      // blaze::submatrix<0UL, 0UL, VectorHDim - 1, ASFrameDim - 1>(I) // next
      // blaze
      blaze::submatrix(I, 0UL, 0UL, VectorHDim - 1, ASFrameDim - 1)
        = vectorspace::identityFrame<
          typename ProjectiveSpace_T::VectorSpace_Type>();
      I(VectorHDim - 1, ASFrameDim - 1) = Unit_Type(1);
      return I;
    }

    template <typename ProjectiveSpace_T>
    auto
    aSpaceFrame(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return blaze::submatrix<0UL, 0UL, ProjectiveSpace_T::VectorDim,
                              ProjectiveSpace_T::ASFrameDim>(frame);
    }

    template <typename ProjectiveSpace_T>
    auto
    vSpaceFrame(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return blaze::submatrix<0UL, 0UL, ProjectiveSpace_T::VectorDim,
                              ProjectiveSpace_T::FrameDim>(frame);
    }

    template <typename ProjectiveSpace_T>
    auto
    pSpaceFrameFromDup(const typename ProjectiveSpace_T::Vector_Type& dir,
                       const typename ProjectiveSpace_T::Vector_Type& up,
                       const typename ProjectiveSpace_T::Point_Type&  origin)
    {

      //      if constexpr (ProjectiveSpace_T::VectorDim == 3) {

      using ReturnType = typename ProjectiveSpace_T::ASFrameH_Type;

      auto asframeh = identityFrame<ProjectiveSpace_T>();
      auto asframe  = blaze::submatrix<0UL, 0UL, ProjectiveSpace_T::VectorDim,
                                      ProjectiveSpace_T::ASFrameDim>(asframeh);

      auto fdir  = blaze::column<0UL>(asframe);
      auto fside = blaze::column<1UL>(asframe);
      auto fup   = blaze::column<2UL>(asframe);
      auto fpos  = blaze::column<3UL>(asframe);

      fdir  = blaze::normalize(dir);
      fup   = blaze::normalize(up - (up * dir) * dir);
      fside = blaze::normalize(blaze::cross(fup, fdir));
      fpos  = blaze::evaluate(origin);

      return ReturnType(asframeh);
      //      }
    }


    namespace detail
    {
      template <typename ProjectiveSpace_T, size_t COL_T>
    auto
      aSpaceFrameColumn(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
      {
        return blaze::column<0UL>(
          blaze::submatrix<0UL, COL_T, ProjectiveSpace_T::VectorDim, 1UL>(
            frame));
      }
    }   // namespace detail

    /*!
     * Returns a frames direction-axis, given a projective frame, with respect
     * to a parent space
     */
    template <typename ProjectiveSpace_T>
    auto
    directionAxis(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return detail::aSpaceFrameColumn<ProjectiveSpace_T,0UL>(frame);
    }

    template <typename ProjectiveSpace_T>
    auto
    sideAxis(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return detail::aSpaceFrameColumn<ProjectiveSpace_T,1UL>(frame);
    }

    template <typename ProjectiveSpace_T>
    auto
    upAxis(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return detail::aSpaceFrameColumn<ProjectiveSpace_T,2UL>(frame);
    }

    template <typename ProjectiveSpace_T>
    auto
    frameOrigin(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return detail::aSpaceFrameColumn<ProjectiveSpace_T,3UL>(frame);
    }


    /*!
     *  Returns a frames direction-axis, in homogenuous coordinates, with
     * respect to a parent space
     */
    template <typename ProjectiveSpace_T>
    auto
    directionAxisH(const typename ProjectiveSpace_T::ASFrameH_Type& frame)
    {
      return blaze::column<0UL>(frame);
    }

    template <typename ProjectiveSpace_T>
    auto sideAxisH(const typename ProjectiveSpace_T::ASFrameH_Type& hframe)
    {
      return blaze::column<1UL>(hframe);
    }

    template <typename ProjectiveSpace_T>
    auto upAxisH(const typename ProjectiveSpace_T::ASFrameH_Type& hframe)
    {
      return blaze::column<2UL>(hframe);
    }

    template <typename ProjectiveSpace_T>
    auto frameOriginH(const typename ProjectiveSpace_T::ASFrameH_Type& hframe)
    {
      return blaze::column<3UL>(hframe);
    }



    // clang-format off
    //   -- Concept TS
    //   template <ProjectiveSpaceConcept&&> void move(...) {}
    //   -- C++20
    //   template <typename ProjectiveSpace_T> void move(...) ->
    //     ProjectiveSpaceConcept<ProjectiveSpace_T> {}
    //   -- Type traits
    //   template <typename ProjectiveSpace_T>
    //     enable_if_t<is_projectivespace<ProjectiveSpace_T>,void>
    //     move(...) {}
    // clang-format on
    template <typename ProjectiveSpace_T>
    void
    translateParent(typename ProjectiveSpace_T::ASFrameH_Type& homogenous_frame,
                    const typename ProjectiveSpace_T::Vector_Type& direction)
    {
      constexpr auto ASFrameDim = ProjectiveSpace_T::ASFrameDim;
      constexpr auto VectorDim = ProjectiveSpace_T::VectorDim;
      blaze::column(
        blaze::submatrix(homogenous_frame, 0UL, 0UL, VectorDim, ASFrameDim), 3UL)
        += direction;
    }

    template <typename ProjectiveSpace_T>
    std::enable_if_t<ProjectiveSpace_T::VectorDim == 3,void>
    rotateParent(typename ProjectiveSpace_T::ASFrameH_Type& frame, double ang,
                 const typename ProjectiveSpace_T::Vector_Type& axis)
    {
      const auto rot_frame = algorithms::rotationMatrix(ang, axis);

      const auto dir  = projectivespace::directionAxis<ProjectiveSpace_T>(frame);
      const auto side = projectivespace::sideAxis<ProjectiveSpace_T>(frame);
      const auto up   = projectivespace::upAxis<ProjectiveSpace_T>(frame);

      blaze::subvector<0UL, 3UL>(blaze::column<0UL>(frame)) = rot_frame * dir;
      blaze::subvector<0UL, 3UL>(blaze::column<1UL>(frame)) = rot_frame * side;
      blaze::subvector<0UL, 3UL>(blaze::column<2UL>(frame)) = rot_frame * up;
    }
  }   // namespace projectivespace


}   // namespace gmlib2::spaces


#endif   // GM2_SPACES_H
