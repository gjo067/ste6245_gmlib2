#ifndef GM2_PROJECTIVESPACEOBJECT_H
#define GM2_PROJECTIVESPACEOBJECT_H

// gmlib2
#include "space.h"

// stl
#include <cassert>
#include <iostream>



namespace gmlib2
{

  template <typename EmbedSpaceInfo_T = spaces::D3R3SpaceInfo<>>
  struct ProjectiveSpaceObject {
    //,
    //                     std::enable_if_t<traits::is_space<ParameterSpaceInfo_T>>>
    //                     {

    // Embedded space
    using EmbedSpaceInfo = EmbedSpaceInfo_T;
    using EmbedSpace     = spaces::ProjectiveSpace<EmbedSpaceInfo>;

    // Dimensions
    static constexpr auto FrameDim   = EmbedSpace::FrameDim;
    static constexpr auto VectorDim  = EmbedSpace::VectorDim;
    static constexpr auto ASFrameDim = EmbedSpace::ASFrameDim;
    static constexpr auto VectorHDim = EmbedSpace::VectorHDim;

    // Embedded space types
    using Unit_Type = typename EmbedSpace::Unit_Type;

    // Embedded space types: vector space
    using Vector_Type = typename EmbedSpace::Vector_Type;
    using Frame_Type  = typename EmbedSpace::Frame_Type;

    // Embedded space types: affine space
    using Point_Type   = typename EmbedSpace::Point_Type;
    using ASFrame_Type = typename EmbedSpace::ASFrame_Type;

    // Embedded space types: projective space
    using PointH_Type   = typename EmbedSpace::PointH_Type;
    using VectorH_Type  = typename EmbedSpace::VectorH_Type;
    using ASFrameH_Type = typename EmbedSpace::ASFrameH_Type;



    // Constructors
    ProjectiveSpaceObject();
    virtual ~ProjectiveSpaceObject() = default;


    //! Return the SpaceObject's projective-space frame;
    //! with respect to parent coordinates
    ASFrameH_Type pSpaceFrameParent() const;

    //! Return the SpaceObject's affine-space frame;
    //! with respect to parent coordinates
    ASFrame_Type aSpaceFrameParent() const;

    //! Return the SpaceObject's vector-space frame;
    //! with respect to parent coordinates
    Frame_Type vSpaceFrameParent() const;


    //! Return the SpaceObject frame's direction-axis;
    //!  with respect to parent space
    const Vector_Type directionAxisParent() const;

    //! Return the SpaceObject frame's side-axis;
    //! with respect to parent space
    const Vector_Type sideAxisParent() const;

    //! Return the SpaceObject frame's up-axis;
    //! with respect to parent space
    const Vector_Type upAxisParent() const;

    //! Return the SpaceObject frame's origin;
    //! with respect to parent space
    const Point_Type  frameOriginParent() const;


    //! Return the SpaceObject frame's direction-axis
    //! in homogeneous coordinates;
    //! with respect to parent space
    const VectorH_Type directionAxisParentH() const;

    //! Return the SpaceObject frame's side-axis in homogeneous coordinates;
    //! with respect to parent space
    const VectorH_Type sideAxisParentH() const;

    //! Return the SpaceObject frame's up-axis in homogeneous coordinates;
    //! with respect to parent space
    const VectorH_Type upAxisParentH() const;

    //! Return the SpaceObject frame's origin in homogeneous coordinates;
    //! with respect to parent space
    const PointH_Type  frameOriginParentH() const;


    //! Apply a translation "locally" to the space object;
    //! before the current transformation stack
    virtual void translateLocal(const Vector_Type& v);

    //! Apply a rotation "locally" to the space object;
    //! before the current transformation stack
    virtual void rotateLocal(double ang, const Vector_Type& axis);

    //! Apply a translation to the space object; with respect to a parent frame
    virtual void translateParent(const Vector_Type& v);

    //! Apply a rotation to the space object; with respect to a parent frame
    virtual void rotateParent(double ang, const Vector_Type& axis);


    //! Set the objects frame; in referrece of a parent space
    //! Right-hand coordinate system
    //! Orthogonal frame: [[dir,side,up],origin]
    virtual void setFrameParent(const Vector_Type& direction_axis,
                                const Vector_Type& up_axis,
                                const Point_Type&  frame_origin);


    // Members
  private:
    /*!
     * Right-hand-system: [dir,side,up,pos]
     * --
     * the projective space object's frame in the projective space
     */
    ASFrameH_Type m_frame{spaces::projectivespace::identityFrame<EmbedSpace>()};
  };










  template <typename EmbedSpaceInfo_T>
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::ProjectiveSpaceObject()
  {
    //    setFrame(m_pos, m_dir, m_up);
  }

  /*!
   * Return the frame of the associated projective space
   */
  template <typename EmbedSpaceInfo_T>
  typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::ASFrameH_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::pSpaceFrameParent() const
  {
    return m_frame;
  }

  template <typename EmbedSpaceInfo_T>
  typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::ASFrame_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::aSpaceFrameParent() const
  {
    return spaces::projectivespace::aSpaceFrame<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Frame_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::vSpaceFrameParent() const
  {
    return spaces::projectivespace::vSpaceFrame<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Vector_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::directionAxisParent() const
  {
    return spaces::projectivespace::directionAxis<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Vector_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::sideAxisParent() const
  {
    return spaces::projectivespace::sideAxis<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Vector_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::upAxisParent() const
  {
    return spaces::projectivespace::upAxis<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Point_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::frameOriginParent() const
  {
    return spaces::projectivespace::frameOrigin<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::VectorH_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::directionAxisParentH() const
  {
    return spaces::projectivespace::directionAxisH<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::VectorH_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::sideAxisParentH() const
  {
    return spaces::projectivespace::sideAxisH<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::VectorH_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::upAxisParentH() const
  {
    return spaces::projectivespace::upAxisH<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::PointH_Type
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::frameOriginParentH() const
  {
    return spaces::projectivespace::frameOriginH<EmbedSpace>(pSpaceFrameParent());
  }



  template <typename EmbedSpaceInfo_T>
  void ProjectiveSpaceObject<EmbedSpaceInfo_T>::translateLocal(
    const Vector_Type& v)
  {
    translateParent(vSpaceFrameParent() * v);
  }

  template <typename EmbedSpaceInfo_T>
  void ProjectiveSpaceObject<EmbedSpaceInfo_T>::rotateLocal(
    double ang, const Vector_Type& axis)
  {
    rotateParent(ang, vSpaceFrameParent() * axis);
  }


  template <typename EmbedSpaceInfo_T>
  void
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::translateParent(const Vector_Type& v)
  {
    spaces::projectivespace::translateParent<EmbedSpace>(m_frame, v);
  }

  template <typename EmbedSpaceInfo_T>
  void
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::rotateParent(double             ang,
                                                        const Vector_Type& axis)
  {


    if constexpr (VectorDim == 3) {
      spaces::projectivespace::rotateParent<EmbedSpace>(m_frame, ang, axis);
    }
  }

  template <typename EmbedSpaceInfo_T>
  void ProjectiveSpaceObject<EmbedSpaceInfo_T>::setFrameParent(
    const Vector_Type& dir, const Vector_Type& up, const Point_Type& origin)
  {

    if constexpr (VectorDim == 3) {

      m_frame = spaces::projectivespace::pSpaceFrameFromDup<EmbedSpace>(dir, up,
                                                                        origin);


//      // subview(s)
//      auto asframe = blaze::submatrix<0UL, 0UL, VectorDim, ASFrameDim>(m_frame);
//      auto fdir    = blaze::column<0UL>(asframe);
//      auto fside   = blaze::column<1UL>(asframe);
//      auto fup     = blaze::column<2UL>(asframe);
//      auto fpos    = blaze::column<3UL>(asframe);

//      // values
//      fdir  = blaze::normalize(dir);
//      fup   = blaze::normalize(up - (up * dir) * dir);
//      fside = blaze::normalize(blaze::cross(fup, fdir));
//      fpos  = blaze::evaluate(origin);
    }
  }

}   // namespace gmlib2

#endif   // GM2_PROJECTIVESPACEOBJECT_H
