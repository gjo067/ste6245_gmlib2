#ifndef GM2_BLAZE_H
#define GM2_BLAZE_H


#include "utils.h"

// blaze
#include <blaze/Math.h>

// stl
#include <chrono>
#include <cmath>
#include <iostream>







//////////////////
// Data structures

namespace gmlib2
{

  // Point/Vector types
  template <typename Unit_T, size_t n, bool TF = blaze::columnVector>
  using VectorT = blaze::StaticVector<Unit_T, n, TF>;

  template <typename Unit_T, size_t n>
  using ColVectorT = VectorT<Unit_T, n>;

  template <typename Unit_T, size_t n>
  using RowVectorT = VectorT<Unit_T, n, blaze::rowVector>;




  // Matrix
  template <typename Unit_T, size_t m, size_t n, bool SO = blaze::rowMajor>
  using MatrixT = blaze::StaticMatrix<Unit_T, m, n, SO>;

  template <typename Unit_T, size_t n, bool SO = blaze::rowMajor>
  using SqMatrixT = MatrixT<Unit_T, n, n, SO>;



  // Extended arithmetic containers
  template <typename Element, bool TF = blaze::columnVector>
  using DVectorT = blaze::DynamicVector<Element, TF>;

  template <typename Element>
  using DColVectorT = DVectorT<Element>;

  template <typename Element>
  using DRowVectorT = DVectorT<Element, blaze::rowVector>;

  template <typename Element, bool SO = blaze::rowMajor>
  using DMatrixT = blaze::DynamicMatrix<Element, SO>;


}   // END namespace gmlib2





////////////////////
// Specialized utils
namespace gmlib2::utils
{
  template <typename Value_T, size_t size_T, bool TF_T>
  constexpr auto
  extendStaticContainer(const blaze::StaticVector<Value_T, size_T, TF_T>& C,
                        const Value_T&                                    val)
  {

    return extendStaticContainer<blaze::StaticVector<Value_T, size_T, TF_T>,
                                 blaze::StaticVector<Value_T, size_T + 1, TF_T>,
                                 size_T, 1, Value_T>(C, val);
  }
}   // namespace gmlib2::utils






/////////////
// Algorithms

namespace gmlib2::algorithms
{

  template <typename Unit_T, size_t n>
  VectorT<Unit_T, n> linearIndependentVector(const VectorT<Unit_T, n>& v)
  {
    if (n == 1 || std::abs(length(v)) < 10e-6)
      return VectorT<Unit_T, n>(Unit_T(0));
    else {
      size_t i, j = 0;
      for (i = 1; i < n; i++)
        if ((v[i] * v[i]) > (v[j] * v[j])) j = i;
      if (j == 0)
        i = 1;
      else
        i = j - 1;

      VectorT<Unit_T, n> r(v);

      Unit_T tmp = -r[j];
      r[j]       = r[i];
      r[i]       = tmp;
      r          = r - (dot(r, v) / dot(v, v)) * v;
      return r;
    }
  }

  template <typename Unit_T>
  DVectorT<Unit_T> linearIndependentVector(const DVectorT<Unit_T>& v)
  {
    const auto n = v.size();

    if (n == 1 || std::abs(length(v)) < 10e-6)
      return DVectorT<Unit_T>(n, Unit_T(0));
    else {
      size_t i, j = 0;
      for (i = 1; i < n; i++)
        if ((v[i] * v[i]) > (v[j] * v[j])) j = i;
      if (j == 0)
        i = 1;
      else
        i = j - 1;

      DVectorT<Unit_T> r(v);

      Unit_T tmp = -r[j];
      r[j]       = r[i];
      r[i]       = tmp;
      r          = r - (dot(r, v) / dot(v, v)) * v;
      return r;
    }
  }


  template <typename Unit_T, size_t n = 3>
  DMatrixT<Unit_T> linearIndependetFrameTo(const DVectorT<Unit_T>& t0)
  {
    static_assert(n == 3, "Neds to be defined in R3");

    auto liv_t0
      = blaze::evaluate(blaze::normalize(linearIndependentVector(t0)));
    auto t2 = blaze::evaluate(blaze::normalize(blaze::cross(liv_t0, t0)));
    auto t1 = blaze::evaluate(blaze::normalize(blaze::cross(t0, t2)));

    DMatrixT<Unit_T> U(n, n);
    blaze::column(U, 0UL) = blaze::normalize(t0);
    blaze::column(U, 1UL) = blaze::normalize(t1);
    blaze::column(U, 2UL) = blaze::normalize(t2);
    return U;
  }

  // rotaion minimization frame - Microsoft - double reflection
  // ToG v.27, n.1, March 2008
  // returns; rhs, [dir,side,up], ([ti,si,ri]), 3x3
  template <typename Unit_T, size_t n>
  DMatrixT<Unit_T> rotationMinimizingFrameMSDR(const DVectorT<Unit_T>& ri,
                                               const DVectorT<Unit_T>& xi,
                                               const DVectorT<Unit_T>& xip1,
                                               const DVectorT<Unit_T>& ti,
                                               const DVectorT<Unit_T>& tip1)
  {
    static_assert(n == 3, "Neds to be defined in R3");

    //    auto ti = blaze::column(Ui,0UL);
    //    auto ri = blaze::column(Ui,2UL);
    //    std::cout << "t[i]: " << std::endl << ti << std::endl;
    //    std::cout << "r[i]: " << std::endl << ri << std::endl;

    auto v1 = xip1 - xi;
    auto c1 = blaze::inner(v1, v1);
    //    std::cout << "v1: " << std::endl << v1 << std::endl;
    //    std::cout << "c1: " << c1 << std::endl;

    auto rLi = ri - ((2.0 / c1) * blaze::inner(v1, ri) * v1);
    auto tLi = ti - ((2.0 / c1) * blaze::inner(v1, ti) * v1);
    //    std::cout << "rL[i]: " << std::endl << rLi << std::endl;
    //    std::cout << "tL[i]: " << std::endl << tLi << std::endl;

    auto v2 = tip1 - tLi;
    auto c2 = blaze::inner(v2, v2);
    //    std::cout << "v2: " << std::endl << v2 << std::endl;
    //    std::cout << "c2: " << c2 << std::endl;

    auto rip1 = rLi - ((2.0 / c2) * blaze::inner(v2, rLi) * v2);
    auto sip1 = blaze::cross(tip1, rip1);
    //    std::cout << "t[i-1]: " << std::endl << tip1 << std::endl;
    //    std::cout << "s[i-1]: " << std::endl << sip1 << std::endl;
    //    std::cout << "r[i-1]: " << std::endl << rip1 << std::endl;

    DMatrixT<Unit_T> Uip1(n, n);
    blaze::column(Uip1, 0UL) = blaze::normalize(tip1);
    blaze::column(Uip1, 1UL) = blaze::normalize(sip1);
    blaze::column(Uip1, 2UL) = blaze::normalize(rip1);
    return Uip1;
  }

  template <typename Unit_T>
  auto ortoNormal(const VectorT<Unit_T, 3>& b, const VectorT<Unit_T, 3>& c)
  {
    VectorT<Unit_T, 3> a = normalize(b - blaze::dot(b, c) * c);
    return a;
  }

  template <typename Unit_T>
  size_t maxAbsIndex(const VectorT<Unit_T, 3>& v)
  {
    size_t j = 0;
    for (size_t i = 1; i < 3; i++)
      if (std::fabs(v[i]) > std::fabs(v[j])) j = i;
    return j;
  }

  /*! SqMatrixT<Unit_T, 3> orthogonalMatrix(const VectorT<Unit_T, 3>& u, const
   * VectorT<Unit_T, 3>& v) \brief To make an orthonormal set of basis-vectors
   * using vector u and vector v as a start.
   *
   *  To make an orthonormal set of basis-vectors using vector u and vector v as
   * a start.
   */
  template <typename Unit_T>
  auto orthogonalMatrix(const VectorT<Unit_T, 3>& u,
                        const VectorT<Unit_T, 3>& v)
  {
    const size_t         n = 3;
    SqMatrixT<Unit_T, n> x;

    VectorT<Unit_T, 3> uu = normalize(u);
    blaze::column<0UL>(x) = uu;
    blaze::column<1UL>(x) = ortoNormal(v, uu);

    size_t ku = maxAbsIndex(u);
    size_t kv = maxAbsIndex(VectorT<Unit_T, 3>{blaze::column<1UL>(x)});

    size_t k = 0, i = 2;
    for (; i < n; i++, k++) {
      if (k == ku) k++;
      if (k == kv) {
        k++;
        if (k == ku) k++;
      }
      blaze::column(x, i)[k] = Unit_T{1};
    }

    for (i = 2; i < n; i++) {
      for (size_t j = 0; j < i; j++) {
        Unit_T tmp = blaze::dot(blaze::column(x, i), blaze::column(x, j));
        blaze::column(x, i) = blaze::column(x, i) - tmp * blaze::column(x, j);
      }
      blaze::column(x, i) = blaze::normalize(blaze::column(x, i));
    }

    using ReturnType = const SqMatrixT<Unit_T, 3>;
    return ReturnType{x};
  }

  template <typename Unit_T, size_t n>
  auto orthogonalMatrixOfAxis(const VectorT<Unit_T, n>& w)
  {
    VectorT<Unit_T, n> lu = linearIndependentVector(w);
    VectorT<Unit_T, n> u  = blaze::cross(lu, w);
    VectorT<Unit_T, n> v  = blaze::cross(w, u);
    //    std::cout << "w:\n" << w << std::endl;
    //    std::cout << "lu:\n" << lu << std::endl;
    //    std::cout << "u:\n" << u << std::endl;
    //    std::cout << "v:\n" << v << std::endl;
    //    const auto axis_check = blaze::cross(u,v);
    //    std::cout << "w(verify):\n" << axis_check << std::endl;

    return orthogonalMatrix(u, v);
  }

  //  template <typename Unit_T, size_t n>
  //  auto basisChange(const SqMatrixT<Unit_T, n>& x, const SqMatrixT<Unit_T,
  //  n>& y)
  //  {
  //    using ReturnType = const SqMatrixT<Unit_T,n>;
  //    return ReturnType{blaze::trans(x) * y};
  //  }

  //  template <typename Unit_T, size_t n>
  //  auto basisChangeInverse(const SqMatrixT<Unit_T, n>& x, const
  //  SqMatrixT<Unit_T, n>& y)
  //  {
  //    using ReturnType = const SqMatrixT<Unit_T,n>;
  //    return ReturnType{y *  x};
  //  }

  // Non-dimensional prototype
  template <typename Unit_T, size_t n, typename = std::enable_if_t<n >= 2UL>>
  auto xyRotationMatrix(Unit_T ang)
  {
    const auto sa = std::sin(ang);
    const auto ca = std::cos(ang);

    using MatType = SqMatrixT<Unit_T, n>;

    MatType M(
      0);   // still rewriting 4 + (N-2UL) values... but who cares -- for now!
    M(0UL, 0UL) = ca;
    M(0UL, 1UL) = -sa;
    M(1UL, 0UL) = sa;
    M(1UL, 1UL) = ca;
    blaze::subvector<2UL, n - 2UL>(blaze::diagonal(M))
      = VectorT<Unit_T, n - 2UL>(1);

    using ReturnType = const MatType;
    return ReturnType{M};
  }



  template <typename Unit_T, size_t n>
  auto rotationMatrix(Unit_T ang, const VectorT<Unit_T, n>& rot_axis)
  {
    // Represent plane of rotation axis
    const auto x = orthogonalMatrixOfAxis(rot_axis);

    // Construct XY rotation matrix
    const auto r = xyRotationMatrix<Unit_T, n>(ang);

    // Rotation matrix is: (r * x)^T * x
    using ReturnType = const SqMatrixT<Unit_T, n>;
    return ReturnType{(x * r) * blaze::trans(x)};
  }

}   // namespace gmlib2::algorithms


#endif   // GM2_BLAZE_H
