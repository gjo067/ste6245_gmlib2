#ifndef GM2_CORE_TRAITS_H
#define GM2_CORE_TRAITS_H



// stl
#include <type_traits>




///////////////////////
// SFINAE HELPERS START
// Substitution failure is not an error

#define GM2_DEFINE_MEMBER_TYPE_CHECK(id)                                       \
  template <typename T, typename = void>                                       \
  struct HasMemberType_##id : std::false_type {                                \
  };                                                                           \
                                                                               \
  template <typename T>                                                        \
  struct HasMemberType_##id<T, decltype(T::id(), void())> : std::true_type {   \
  };
#define GM2_HAS_MEMBER_TYPE(T, id) HasMemberType_##id<T>::value



#define GM2_DEFINE_MEMBER_CHECK(id)                                            \
  template <typename T, typename = void>                                       \
  struct HasMember_##id : std::false_type {                                    \
  };                                                                           \
                                                                               \
  template <typename T>                                                        \
  struct HasMember_##id<T, decltype(std::declval<T>().id, void())>             \
    : std::true_type {                                                         \
  };

#define GM2_HAS_MEMBER(T, id) HasMember_##id<T>::value

// SFINAE HELPERS END
/////////////////////

namespace gmlib2::traits
{



  //////////////////////////////////
  // Space concepts (C++20)
  //  template <typename FrameDim_T, typename VectorDim_T>
  //  concept Space = std::is_integral<FrameDim_T>::value and
  //          std::is_integral<VectorDim_T>::value and
  //          requires(FrameDim_T FD, VectorDim_T VD){
  //    frameDimension() const -> FrameDim_T;
  //    vectorDimension() const -> VectorDim_T;
  //  };

  //  template <typename FrameDim_T>
  //  concept ParametricSpace = Space<FrameDim_T,1>;

  //  template <typename FrameDim_T,VectorDim_T>
  //  concept EmbeddedSpace = Space<FrameDim_T,VectorDim_T>;


  //////////////////////////////////
  // Space type-trait
  //  namespace traits {

  //    // Space trait
  //    template <typename Space_T>
  //    class is_space {
  //      using yes = char;
  //      using no  = long;

  //      template <typename C>
  //      static yes hasFrameDimFunc(typeof(&C::frameDim));
  //      template <typename C>
  //      static no hasFrameDimFunc(...);

  //      template <typename C>
  //      static yes hasVectorDimFunc(typeof(&C::vectorDim));
  //      template <typename C>
  //      static no hasVectorDimFunc(...);


  //      template <typename C>
  //      static constexpr bool hasRequiredMemberFunctions() {
  //        return (sizeof(hasFrameDimFunc<Space_T>(0)) == sizeof(yes))
  //                and (sizeof(hasVectorDimFunc<Space_T>(0)) == sizeof(yes));
  //      }

  //    public:
  //      enum {
  //        value = hasRequiredMemberFunctions()
  //      };
  //    };
  //  }



//  template <typename SpaceInfo_T>
//  struct IsSpaceInfo {

//    GM2_DEFINE_MEMBER_TYPE_CHECK(Unit_Type)
//    GM2_DEFINE_MEMBER_CHECK(VectorDim)
//    GM2_DEFINE_MEMBER_CHECK(FrameDim)

//    static const auto value = GM2_HAS_MEMBER_TYPE(SpaceInfo_T, Unit_Type)
//                              and GM2_HAS_MEMBER(SpaceInfo_T, VectorDim)
//                              and GM2_HAS_MEMBER(SpaceInfo_T, FrameDim);
//  };


//  template <typename Space_T>
//  struct IsVectorSpace {

//    GM2_DEFINE_MEMBER_CHECK(VectorDim)
//    GM2_DEFINE_MEMBER_CHECK(FrameDim)

//    static const auto value = GM2_HAS_MEMBER(Space_T, VectorDim)
//                              and GM2_HAS_MEMBER(Space_T, FrameDim);
//    //! \todo: add the reset of vector space definitions
//  };

//  template <typename Space_T>
//  struct IsAffineSpace {

//    //! \todo:  HAS VECTOR SPACE

//    GM2_DEFINE_MEMBER_CHECK(ASFrameDim)

//    static const auto value = GM2_HAS_MEMBER(Space_T, ASFrameDim);
//    //! \todo: add the reset of affine space definitions
//  };

//  template <typename Space_T>
//  struct IsProjectiveSpace : IsAffineSpace<Space_T> {

//    GM2_DEFINE_MEMBER_CHECK(VectorDimH)

//    static const auto value
//      = IsAffineSpace<Space_T>::value and GM2_HAS_MEMBER(Space_T, VectorDimH);
//    //! \todo: add the reset of projective space definitions
//  };

}   // namespace gmlib2::traits





#endif   // GM2_CORE_TRAITS_H
