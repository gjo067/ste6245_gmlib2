#ifndef GM2_GMLIB2_H
#define GM2_GMLIB2_H

#include "platform.h"

// core
#include "core/gm2_blaze.h"
#include "core/bernsteinbasisgenerators.h"
#include "core/hermitebasisgenerators.h"
#include "core/space.h"
#include "core/projectivespaceobject.h"
#include "core/utils.h"

// parametrics
#include "parametric/ppoint.h"
#include "parametric/pcurve.h"
#include "parametric/psurface.h"
#include "parametric/parametricobject.h"
#include "parametric/psubspaceobject.h"
// -- polygon construction -- DEV
#include "parametric/ppolygonsurfaceconstruction.h"
#include "parametric/polygon_surface_constructions/ppolygonconstruction.h"
#include "parametric/polygon_surface_constructions/pgeneralizedbezierpatchconstruction.h"
// -- stuff -- non [grouped or sorted]
#include "parametric/stuff/gbcutils.h"
#include "parametric/stuff/utils.h"
// -- ** polygon surfaces
#include "parametric/stuff/polygons/ppolygon.h"
#include "parametric/stuff/polygons/pgeneralizedbezierpatch.h"
// -- classic shapes
// -- ** curves
#include "parametric/classic_shapes/pcircle.h"
#include "parametric/classic_shapes/pline.h"
// -- ** surfaces
#include "parametric/classic_shapes/pplane.h"
#include "parametric/classic_shapes/psphere.h"
#include "parametric/classic_shapes/pcylinder.h"
#include "parametric/classic_shapes/ptorus.h"
// -- classic constructions
// -- ** curves
#include "parametric/classic_constructions/pbeziercurve.h"
#include "parametric/classic_constructions/phermitecurve.h"
// -- ** surfaces
#include "parametric/classic_constructions/pbeziersurface.h"
#include "parametric/classic_constructions/pbicubiccoonspatch.h"
#include "parametric/classic_constructions/pbilinearcoonspatch.h"

#endif   // GM2_GMLIB2_H
