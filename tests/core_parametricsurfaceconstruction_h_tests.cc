// gtest
#include <gtest/gtest.h>   // googletest header file

// gmlib2
#include <parametric/polygon_surface_constructions/ppolygonconstruction.h>

using namespace gmlib2;
using namespace gmlib2::parametric;






namespace unittest_detail
{
} // namespace unittest_detail










TEST(Core_ParametricSurfaceConstruction, Stack_Compile_Test)
{
  using EmbedSpaceInfo = spaces::D3R3SpaceInfo<>;
  using SO             = ProjectiveSpaceObject<EmbedSpaceInfo>;
  using PPolygon       = polygon_surface_constructions::PPolygon<SO>;


  // Space object
  [[maybe_unused]] SO so;

  // Polygon surfaces construction
  [[maybe_unused]] PPolygon ppolygon4{utils::parametric::psc::generateRegularPolygon2DXZ(4)};
  [[maybe_unused]] PPolygon ppolygon5{utils::parametric::psc::generateRegularPolygon2DXZ(5)};
  [[maybe_unused]] PPolygon ppolygon6{utils::parametric::psc::generateRegularPolygon2DXZ(6)};

  EXPECT_EQ(ppolygon4.sides(),4UL);
  EXPECT_EQ(ppolygon5.sides(),5UL);
  EXPECT_EQ(ppolygon6.sides(),6UL);
}
