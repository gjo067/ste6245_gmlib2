// gtest
#include <gtest/gtest.h>   // googletest header file

// gmlib2
#include <parametric/classic_shapes/pplane.h>
#include <parametric/classic_shapes/psphere.h>
#include <parametric/classic_shapes/ptorus.h>
using namespace gmlib2::parametric;

// stl
#include <chrono>
using namespace std::chrono_literals;


namespace unittest_detail
{

  template <typename PSurface_T>
  void invokeDefaultPSurfaceMethods([[maybe_unused]]const PSurface_T& surface)
  {
    static_assert(std::is_base_of<PSurface<>, PSurface_T>::value,
                  "PSurface_T must be a base of PSurface<>.");

    //  [[maybe_unused]] auto is_c_u_res    = surface.isClosedU();
    //  [[maybe_unused]] auto is_c_v_res    = surface.isClosedV();
    //  [[maybe_unused]] auto param_u_s_res = surface.parameterUStart();
    //  [[maybe_unused]] auto param_u_e_res = surface.parameterUEnd();
    //  [[maybe_unused]] auto param_v_s_res = surface.parameterVStart();
    //  [[maybe_unused]] auto param_v_e_res = surface.parameterVEnd();
    //  [[maybe_unused]] auto local_space_eval_result
    //    = surface.evaluateLocal(param_u_s_res, param_v_s_res, 0, 0, false,
    //    false);
    //  [[maybe_unused]] auto parent_space_eval_result
    //    = surface.evaluateParent(param_u_s_res, param_v_s_res, 0, 0, false,
    //    false);
  }

}   // namespace unittest_detail

TEST(PPlaneForceCompile, DISABLED_CompileTest)
{
  PPlane<> plane;
  unittest_detail::invokeDefaultPSurfaceMethods(plane);
  EXPECT_TRUE(true);
}

TEST(PSphereForceCompile, DISABLED_CompileTest)
{
  PSphere<> sphere;
  unittest_detail::invokeDefaultPSurfaceMethods(sphere);
  EXPECT_TRUE(true);
}

TEST(PTorusForceCompile, DISABLED_CompileTest)
{
  PTorus<> torus;
  unittest_detail::invokeDefaultPSurfaceMethods(torus);
  EXPECT_TRUE(true);
}
